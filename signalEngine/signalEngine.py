#-*-coding: utf-8-*-
# coding: utf-8
import re
import codecs
from bs4 import BeautifulSoup
import urllib2
import sys
import pymysql
import time
import datetime
from dateutil import parser
import socket
import rsa
from Crypto.Cipher import AES
from Crypto import Random
import thread
import random
import threading


#sys.stdout = codecs.getwriter('euc-kr')(sys.stdout)

PORT = 4116 # api server port

# api server public key
pub_key = rsa.key.PublicKey(92025398516352833057483421158458980440871913197008455920483103521642106550992767915348696182601869258650607082222580330812659937930549384320678097717145680841077608291666361051972681816577132759564739651756921434893798432860408603418671962155309410319207569340012253716665444699734698772857826603833986134207,65537)

current_random_key = ""
semaphore = 0

L = threading.Lock()

conn = pymysql.connect(host = '127.0.0.1', port = 3306, user= 'soma', passwd = 'thak2013', database = 'stock',autocommit=True) # connect to mysql database
cur = conn.cursor()

def padding(msg):	# padding for AES encryption ( only multiple 16byte )
	block_size = 32
	padding = ' '
	forPadding = block_size-(len(msg)%block_size)
	msg += padding*forPadding
	return msg

def dongsam_encrypt(msg): # AES encryption after random key make and  RSA encryption
	global pub_key
	global current_random_key
	global semaphore
	#padding
	msg = padding(msg)

	if semaphore==0:
		random_key = Random.new().read(32)
		current_random_key = random_key
	else:
		random_key = current_random_key
	semaphore+=1
	encrypted_key = rsa.encrypt(random_key, pub_key)
	
	cipher = AES.new(random_key)

	encrypt_msg = cipher.encrypt(msg)

	return_msg = encrypted_key+"|+|"+encrypt_msg
	return return_msg

def dongsam_client_decrypt(msg): # AES decrypt using shared once random_key
	global current_random_key
	global semaphore
	cipher = AES.new(current_random_key)
	semaphore -= 1
	decrypt_msg = cipher.decrypt(msg.rstrip())
	return decrypt_msg


def getPrice(company_id): # get company stock price realtime to api server 
	global L
	L.acquire()
	
	try:
		client_socket.send(dongsam_encrypt(str("%06d"%company_id)))
		price = dongsam_client_decrypt(client_socket.recv(1024))
	except:
		print "getPrice error ( decrypt or connection)"
		price = 0 # if price==0 error
	if price == 0:
		print "price 0 error..."
	
	L.release()
	return price


def getPrice_NE(company_id): # get company stock price realtime to api server 
	global L
	L.acquire()
	try:
		client_socket.send(str("%06d"%company_id))
		price = client_socket.recv(1024)
	except:
		print "getPrice error ( decrypt or connection)"
		price = 0 # if price==0 error
	if price == 0:
		print "price 0 error...2"
	L.release()	
	# print "NE_", price
	return price

def getAccount_money():
	global cur
	try:
		cur.execute("select account_money from TRADING ORDER BY trading_id DESC LIMIT 1")
		for row in cur:
			account_money = int(row[0])
		return account_money
	except:
		return 0

# after diff ,value union
def trading(company_id, signal_diff, signal_value): # stock trading, buy and sell   #트레이딩 엔진부 
	global cur
	global L
	weight = 10 # signal 계산을 위한 가중치
	account_money = getAccount_money() #현재 계좌 소유금액 가져오기
	if account_money == 0 :
		return
	# cur.execute("select account_money from TRADING ORDER BY trading_id DESC LIMIT 1")
	# for row in cur:
	# 	account_money = int(row[0])
	
	# price = int(getPrice(company_id))
	price = int(getPrice_NE(company_id)) #해당 company의 현재싯가를 API 통해 받아옴 
	
	if not price:
		return
	print "trading", price

	# trading_value =  signal_diff * ((signal_value+100) / 100)

	print "=============="
	print "signal = ", signal_diff, signal_value
	trading_value =  float(signal_diff) * ((float(signal_value) + 100) / 100) * weight   # 실시간시그널변화량 * 오늘누적시그널값 * 가중치
	print "trading value = ",trading_value
	trading_total_price = (account_money / 100) * trading_value # 구매할 금액을 정하는 과정, 총 보유 금액의 1/100 * 위에서구한 trading value를 곱해준다
	if trading_total_price == 0 or price == 0:
		print "divide zero error "
		return
	trading_quantity = trading_total_price / price # 구매할 금액에서 한주당 가격을 나눠, 총 몇주를 구매하는지 산정
	trading_buy_money = price * trading_quantity  
	print "trading_quantity = ",trading_quantity
	print "trading_buy_money = ",trading_buy_money


	try: # 구매 내역을 DB에 저장
		cur.execute("insert into TRADING (trading_type, trading_company_id, trading_price, trading_quantity, trading_time, account_money , trading_value) values (%s,%s,%s,%s,%s,%s,%s)", (1, company_id, price, trading_quantity, datetime.datetime.now()-demoTimedelta , account_money, trading_value ))
	except:
		print "trading buy error"
		return

	try: # 방금 거래에대한 trade id 를 얻음
		cur.execute("select trading_id from TRADING ORDER BY trading_id DESC LIMIT 1")
		for row in cur:
			sell_trading_id = int(row[0])
	except:
		print "trading buy error2"
		return

	while True: #  지속적으로 시세변화를 측정하여 적정시기에 매도, 수익 실현
		sleepTime = int(random.uniform(20,40)) # 초당 API 횟수제한을 피하기위해 20~40초 중 랜덤한 주기로 지속적 요청
		after_price = int(getPrice_NE(company_id)) # 현재 주식 가격 얻어오기
		if not after_price:
			time.sleep(sleepTime)
			continue

		#현재 시세에서 판매시 판매금액 산정해봄
		trading_sell_money = after_price * trading_quantity

		# print trading_value, trading_total_price, trading_quantity, trading_buy_money, after_price, trading_sell_money
		middle_account_money = getAccount_money()
		#현재 계좌 잔량 조회
		if middle_account_money == 0:
			continue
		# cur.execute("select account_money from TRADING ORDER BY trading_id DESC LIMIT 1")
		# for row in cur:
		# 	middle_account_money = int(row[0])

		delta_account_money = middle_account_money - account_money # 지속적 쓰레딩중 계좌잔량이 변할 수 있으니 델타값으로 계산

		after_account_money = account_money - trading_buy_money + trading_sell_money 
		real_account_money = after_account_money + delta_account_money
		#판매금액과 현재 주식값을 빼서 순수익을 계산

		percent = after_account_money / account_money * 100 - 100 #수익 퍼센트 계산
		profit = trading_sell_money - trading_buy_money  #수익 금액



		# 수익이 0.3 프로 이상 났거나, 손실이 0.5프로 이상 났을때 판매 하여 수익실현 혹은 손절
		if percent > 0.2:  

			if trading_quantity < 0 or real_account_money < 0:
				continue

			try:
				cur.execute("insert into TRADING (trading_type, trading_company_id, trading_price, trading_quantity, trading_time, account_money, sell_trading_id, trading_value, sell_percent, sell_profit, sell_return) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", (0, company_id, after_price, trading_quantity, datetime.datetime.now()-demoTimedelta , real_account_money , sell_trading_id , trading_value, percent, profit, 1))
				#판매 기록 등록
			except:
				print "trading  sell error"
				return

			try:
				cur.execute("update TRADING set sell_return=1 where trading_id = %s",( sell_trading_id ))
				#판매 완료로 셋
			except:
				print "trading  sell error2"
				return
			print "++++++++++++++++++++++++++++++"
			print " sell complete !! get", profit
			print " percent ", percent, "%"
			print "++++++++++++++++++++++++++++++"
			return

		elif percent < -0.5:

			if trading_quantity < 0 or real_account_money < 0:
				continue

			try:
				cur.execute("insert into TRADING (trading_type, trading_company_id, trading_price, trading_quantity, trading_time, account_money, sell_trading_id, trading_value, sell_percent, sell_profit, sell_return) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", (0, company_id, after_price, trading_quantity, datetime.datetime.now()-demoTimedelta , real_account_money , sell_trading_id , trading_value, percent, profit, 1))
				#판매 기록 등록
			except:
				print "trading  sell error"
				return

			try:
				cur.execute("update TRADING set sell_return=1 where trading_id = %s",( sell_trading_id ))
				#판매 완료로 셋
			except:
				print "trading  sell error2"
				return
			print "------------------------------"
			print " sell  T_T lose ", profit
			print " percent ", percent, "%"
			print "------------------------------"
			return

		else:
			print ".",
			time.sleep(sleepTime) # delay before sell the stock
			# 초당 API 횟수제한을 피하기위해 20~40초 중 랜덤한 주기로 지속적 요청
			continue

def get_signal_value(company_id): # signal Engine 이 장중에 재시작시 db에 저장된 signal value 가져오기 위한 함수 
	global cur
	cur.execute("select signal_value from STOCKSIGNAL WHERE signal_company_id = %s and DATE(signal_time) = DATE(NOW()) ORDER BY signal_id DESC LIMIT 1",(company_id))
	last_signal_value = 0
	for row in cur:
		last_signal_value = int(row[0])
	print "last_signal_value",last_signal_value
	return last_signal_value
	

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # TCP
client_socket.connect(("1.234.36.27", PORT)) # connect to api server


comDict = {} #dictionary for companies signal management realtime

minuteCheck = 0
while True:
	# demoTimedelta = datetime.timedelta( days=2, hours=-2, minutes=50 ) # for test to past stock market
	demoTimedelta = datetime.timedelta( days=0, hours=0, minutes=0 ) # 과거에대해 엔진을 돌리기위한 델타값 설정
	startTime = datetime.datetime.now() - datetime.timedelta(seconds=3) - demoTimedelta 
	endTime = datetime.datetime.now() + datetime.timedelta(seconds=3) - demoTimedelta
	

	# 오피니언 실시간 조회
	# try:
		# select opinion_company_id, opinion_value, opinion_date from OPINION where useFlag = 0 and opinion_date > '2013-11-13 00:10:02' and opinion_date < '2013-11-13 00:10:06'
	try:
		cur.execute("select opinion_id, opinion_company_id, opinion_value, opinion_date from OPINION where useFlag = 0 and opinion_date > %s and opinion_date < %s",( startTime, endTime ))
	except:
		continue

	for row in cur:
		try:
			opinion_id = row[0]	
			opinion_company_id = row[1]
			opinion_value = int(row[2])
			opinion_date = row[3]
		except:
			break
		print opinion_id, opinion_company_id, opinion_value, opinion_date
		try:
			cur.execute("update OPINION set useFlag=1 where opinion_id = %s",( str(opinion_id) ))	#사용한 오피니언이라고 flag set
		except:
			break
		if not opinion_company_id in comDict:	# 시그널 케시 딕트에 해당 기업이 없으면
			last_signal_value = get_signal_value(int(opinion_company_id))	#오늘 해당기업에대한 시그널 발생했었는지에대해 디비에서 최신값 조회
			if last_signal_value:	#있었다면, 해당 누적 값 + 현재 시그널값
				comDict[opinion_company_id] = last_signal_value + opinion_value
			else:	#없었다면 오늘의 초기 시그널이니 방금 발생한 시그널값으로 셋
				comDict[opinion_company_id] = opinion_value
		else:	# 증감
			comDict[opinion_company_id] += opinion_value
		cur.execute("insert into STOCKSIGNAL (signal_company_id, signal_diff, signal_value, signal_time) values (%s,%s,%s,%s)", (opinion_company_id, opinion_value, comDict[opinion_company_id], datetime.datetime.now()-demoTimedelta ))
		#생성된 시그널 디비에 입력
		if 0 < opinion_value and 0 < comDict[opinion_company_id]: 
			# price = getPrice(opinion_company_id)
			price = getPrice_NE(opinion_company_id)
			if price: # 트레이딩하는 주식 별 쓰레드를 새로 생성하여 주기적 체크
				thread.start_new_thread( trading, (opinion_company_id,opinion_value,comDict[opinion_company_id]) )
				# trading(opinion_company_id,opinion_value,comDict[opinion_company_id])
			else:
				print "not price error"
				continue


	# except:
	# 	print "aleady or error" 

	# print "sleep start", startTime
	print startTime, "To + 6 seconds"
	time.sleep(1) # 1초씩 텀 
	minuteCheck += 1
	if minuteCheck%1800 == 0: # 30 분마다 시그널이 0을향해 증가 혹은 감소되게 set
		for key in comDict.iterkeys():
			if comDict[key] < 0:
				comDict[key] += 1
			elif comDict[key] > 0:
				comDict[key] -= 1
	#print comDict
	# print "sleep end", endTime


	# client_socket.send(dongsam_encrypt(data))
	# price = dongsam_client_decrypt(client_socket.recv(1024))



client_socket.close()
