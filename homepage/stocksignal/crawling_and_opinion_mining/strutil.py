#-!- coding: utf-8 -!-
import StringIO


ENG_SUFF_TAB = [
	('ily', [(3, 'y')]),
	('ly', [(2, '')]),
	('ed', [(2, ''), (1, '')]),
	('d', [(1, '')]),
	('es', [(2, ''), (1, '')]),
	('s', [(1, '')]),
	('ier', [(3, 'y')]),
	('er', [(2, ''), (1, '')]),
	('or', [(2, '')]),
	('ar', [(2, '')]),
	('ist', [(3, 'y')]),
	('st', [(2, '')]),
]



def eng_stem_list(word):
	res = []
	for suff, forms in ENG_SUFF_TAB:
		if word.endswith(suff):
			for form in forms:
				stem = word[:-form[0]] + form[1]
				res.append(stem)
	return res

def is_normal_ch(word):
	for c in word:
		if ord(c) > 128:
			return False
	return True

def is_ascii_ch(word):
	for c in word:
		if ord(c) > 255:
			return False
	return True

def is_hangul_ch(word):
	v = ord(word[0])
	if 0xAC00 <= v and v <= 0xD7FB:
		return True
	return False

def word_split_en(p):
	''' get list of English words only '''
	RES = []
	flds = p.split()
	for f in flds:
		if is_normal_ch(f) and f.isalpha():
			if not f.isupper():
				f = f.lower()
				RES.append(f)
	return RES


def token_split_en(p):
	''' get list of English words only '''
	RES = []
	i = 0
	while i < len(p):
		s = StringIO.StringIO()
		while i < len(p) and ord(p[i]) < 128 and p[i].isalpha():
			s.write(p[i])	
			i += 1
		w = s.getvalue()
		if len(w) > 1 and (i==len(p) or ord(p[i]) < 128):
			# if it is NOT that all chars in a word is upper-case,
			# make it lower-case.
			if not w.isupper():
				w = w.lower()
			RES.append(w)

		while i < len(p) and not (ord(p[i]) < 128 and p[i].isalpha()):
			if p[i]=='(': # stop there if we meet '('
				return RES
			i += 1
	return RES


def token_split_ko(p):
	''' get list of English words only '''
	RES = []
	i = 0
	while i < len(p):
		s = StringIO.StringIO()
		while i < len(p):
			v = ord(p[i])
			if not (0xAC00 <= v and v <= 0xD7FB):
				break
			s.write(p[i])	
			i += 1
		w = s.getvalue()
		if len(w) > 0:
			RES.append(w)

		while i < len(p):
			v = ord(p[i])
			if (0xAC00 <= v and v <= 0xD7FB):
				break
			i += 1
	return RES

## Hangul, Unicode
KBD = [
	"1234567890-=",
	"qwertyuiop[]",
	"asdfghjkl;'",
	"zxcvbnm,./"
]

JAMO1 = [
'r', 'R', 's', 'e', 'E', 'f', 'a', 'q', 'Q', 't', 'T', 'd', 'w', 'W', 'c', 'z', 'x', 'v', 'g'
]

JAMO2 = [
'k', 'o', 'I', 'O', 'j', 'p', 'u', 'P', 'h', 'hk', 'ho', 'hl',
'y', 'n', 'nj', 'np', 'nl', 'b', 'm', 'ml', 'l',
]

JAMO3 = [
'', 'r', 'R', 'rt', 's', 'sw', 'sg', 'e', 'f', 'fr',
'fa', 'fq', 'ft', 'fx', 'fv', 'fg', 'a', 'q', 'qt', 't',
'T', 'd', 'w', 'c', 'z', 'x', 'v', 'g',
]

def split_hancode(v):
	''' v: unicode value of Hangul char
		splited: f,s,t (1st, 2nd, 3rd)
	'''
	v = v - 0xAC00
	third = v % 28
	tmp = (v - third) / 28
	second = tmp % 21
	first = tmp / 21
	return first, second, third

def merge_hancode(f, s, t):
	return f*588 + s*28 + t + 0xAC00

def hancode2kdbchar(hv):
	f,s,t = split_hancode(hv)
	try:
		res = '%s%s%s' % (JAMO1[f], JAMO2[s], JAMO3[t])
		return res
	except:
		return '...'

def hanword2kdbword(hword):
	''' convert a Hangul word into a Keyborad word 
		한국 --> gksrnr
		* hword must be unicode.
	'''
	hvals = map(ord, hword)
	kbdchars = map(hancode2kdbchar, hvals)
	res = ''.join(kbdchars)
	return res

def get_lang(word):
	''' identify language of a word: simple one
	'''
	for ch in word:
		v = ord(ch)
		if 0xAC00 <= v and v <= 0xD7FB:
			return 'Hangul'
		if 0x80 <= v and v <= 0xFF:
			return 'Latin'
		if v > 0xFF:
			lang = pyhan.unicode.code2script.get_script(v)
			return lang
	return 'English'


if __name__=="__main__":
	print eng_stem_list('prettier')
	print eng_stem_list('potatoes')

	a = split_hancode(0xAC15) # 강
	print a
	b = merge_hancode(a[0], a[1], a[2])
	d = split_hancode(b)
	print d
	c = unicode(b)
	print '%X'% b, type(c)
	print c.encode('utf-8')
