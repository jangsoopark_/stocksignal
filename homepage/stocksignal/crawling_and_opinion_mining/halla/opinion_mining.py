# coding: utf-8

import sys

def encode(string, encoding="utf-8"):
	if isinstance(string, str):
		return string
	elif isinstance(string, unicode):
		return string.encode(encoding)

	return string

def decode(string, encoding="utf-8"):
	if isinstance(string, str):
		return string.decode(encoding)
	elif isinstance(string, unicode):
		return string

	return string # 위의 두 가지 경우가 아닐 때. 하지만 이런 경우는 없다고 보는게 편하겠지?

def colored(content, color="yellow"):
	colors = {"red": 91, "blue": 94, "green": 92, "yellow": 93, "pink": 95}

	if color in colors:
		return "\033[{}m{}\033[0m".format(colors[color], content)
	else:
		return colored(content, "yellow")

class OpMi_Base(object):
	pass
	# def _find_words(self, ... # 추가 하자. 귀찮다. ... 하려고 했으나 형식이 미묘하게 달라서 일단 보류. 역시 귀찮아

class OpMi_DataManager(OpMi_Base):
	def _load_data_from_file(self, file_name, data):
		try:
			file = open(file_name, "rt")
		except IOError:
			print "error: cannot open file %s" % (file_name)

			return False
		# data = [] # 이렇게 하면 작동이 안 되는 것 같아서 주석 처리해둠.

		for line in file:
			stripped_line = line.strip()
			if stripped_line:
				data.append(stripped_line)

		file.close()

		return True

class OpMi_OpinionFinder(OpMi_DataManager):
	def __init__(self, positive_words_file_name=None, negative_words_file_name=None):
		result = True

		self.positive_words = []
		self.negative_words = []

		if positive_words_file_name:
			result &= self._load_data_from_file(positive_words_file_name, self.positive_words)
		if negative_words_file_name:
			result &= self._load_data_from_file(negative_words_file_name, self.negative_words)

		if result is False:
			print colored("an error occurred while loading data from files.", "red")

	def feed(self, content):
		result = [{}, {}]

		result[0] = self._find_opinion_words(content, self.positive_words)
		result[1] = self._find_opinion_words(content, self.negative_words)

		return result

	def _find_opinion_words(self, content, opinion_words):
		result = {}

		content = decode(content)

		for opinion_word in opinion_words:
			for possible_combination in self._possible_combinations(opinion_word):
				possible_combination = decode(possible_combination)
				length = len(possible_combination)

				next_content = content

				while True:
					index = next_content.find(possible_combination)
					if index >= 0:
						if opinion_word in result:
							result[opinion_word][0] += 1
							result[opinion_word][1].append(index)
						else:
							result[opinion_word] = [1, [index]]

						next_content = next_content[index + length:]
					else:
						break

		return result

	def _combine_words(self, words, start_index, count):
		result = []

		length = len(words)

		if isinstance(words, (tuple, list)):
			temp = ""

			i = 0
			while i < length:
				if i >= start_index and i < start_index + count:
					temp += words[i]
				else:
					if temp is not "":
						result.append(temp)

						temp = ""

						i -= 1
					else:
						result.append(words[i])
				i += 1
			if temp is not "":
				result.append(temp)

		return " ".join(result)

	def _possible_combinations(self, combined_word):
		result = []

		if isinstance(combined_word, str):
			words = combined_word.split()
			length = len(words)

			result.append(self._combine_words(words, 0, 0))

			for i in range(2, length + 1):
				for j in range(0, length - i + 1):
					result.append(self._combine_words(words, j, i))

		return result

class OpMi_CompanyFinder(OpMi_DataManager):
	def __init__(self, companies_file_name=None):
		result = True

		self.companies = []

		if companies_file_name:
			result &= self._load_data_from_file(companies_file_name, self.companies)

		if result is False:
			print colored("an error occurred while loading data from files.", "red")

	def feed(self, content):
		result = self._find_companies(content)

	def _find_companies(self, content):
		result = {}

		content = decode(content)

		for company in self.companies:
			company = decode(company)
			length = len(company)

			next_content = content

			while True:
				index = next_content.find(company)
				if index >= 0:
					if encode(company) in result:
						result[encode(company)][0] += 1
						result[encode(company)][1].append(index)
					else:
						result[encode(company)] = [1, [index]]

					next_content = next_content[index + length:]
				else:
					break

		return result

def sort_key(x):
	return x[1]

if len(sys.argv) > 1:
	op_finder = OpMi_OpinionFinder("../positive_words.txt", "../negative_words.txt")
	cp_finder = OpMi_CompanyFinder("../companies.txt")

	content = sys.argv[1]

	result = op_finder.feed(content)

	positive_score = 0
	negative_score = 0

	positive = colored("positive", "blue")
	negative = colored("negative", "red")

	print

	for (positive_word, data) in result[0].iteritems():
		print colored(positive_word, "blue"), data[0]

		positive_score += data[0]

	for (negative_word, data) in result[1].iteritems():
		print colored(negative_word, "red"), data[0]

		negative_score += data[0]

	result = cp_finder.feed(content)

	sorted_result = sorted([(k, v[0]) for (k, v) in result.iteritems()], key=sort_key, reverse=True)

	for company, frequency in sorted_result:
		print colored(company), frequency

	print "-" * 60
	
	print "%s score is %s." % (positive, colored(positive_score, "green"))
	print "%s score is %s." % (negative, colored(negative_score, "green"))
	print "so this is %s news of %s." % (positive if positive_score > negative_score else negative, colored(sorted_result[0][0]))

	print
else:
	print "Usage: python {} [news_content]".format(sys.argv[0])