#-*-coding: utf-8-*-
# coding: utf-8
import re
import codecs
from bs4 import BeautifulSoup
import urllib2
import sys
import collections
import strutil

sys.stdout = codecs.getwriter('utf-8')(sys.stdout)

JOSA = [
u"은", u"는", u"이", u"가",
u"을", u"를", 
u"의", u"와", u"과", u"및", u"에게", u"엔", u","

]


def analyze(text):
	# words = strutil.token_split_ko(text)
	words = text.split()
	wCnter = collections.Counter(words)
	most = wCnter.most_common(10)
	for w,freq in most:
	#for w,freq in wCnter.iteritems():
		print w, freq

def webtotext(paged_url):
	#wf = codecs.open(fname, 'w', encoding='utf-8')
	paged_html = urllib2.urlopen(paged_url)
	paged_html_result = paged_html.read()
	soup = BeautifulSoup(paged_html_result,'lxml' ,from_encoding='utf-8')
	[s.extract() for s in soup('script')] # script, 자바스크립트 등 제거
	[s.extract() for s in soup('style')]	# CSS 등 style 코드 제거 
	return soup.get_text()

def trimtext(text):
	text = text.replace("\r\n","\n")
	return text.replace("\n\n","\n")
	
def lenthofeachline(text):
	lines = text.splitlines()
	lengths = [len(line) for line in text.splitlines()]
	
	lines = sorted(lines,key= lambda line:len(line),reverse=True)
	print lines[0]
	print "====================="
	print lines[1]

def getmaintext(text):
	lines = text.splitlines()
	# lines = sorted(lines,key= lambda line:len(line),reverse=True)
	lines = filter(lambda line: len(line)>100, lines)
	return "\n".join(lines)


class LookupCompany(object):
	def __init__(self):
		self.codeD = {}
		self.companyD = {}
		self.opinionD = {}
	

	def loadcompanylist(self,fname):
		
		fp = codecs.open(fname,encoding='utf-8')
		for line in fp:
			splited = line.split('|')
			code = splited[0].strip()
			company = splited[1].strip()
			
			self.codeD[code] = company
			self.companyD[company] = code
			#print code,company

	def loadopinionwords(self,fname,value):
		fp = codecs.open(fname,encoding='utf-8')
		for line in fp:
			if len(line.strip())==0:
				continue
			self.opinionD[line.strip()] = value
		fp.close()	

	def checkcompany(self, word):
		# if word in self.companyD:
		# 	return True
		
		for company in self.companyD:
			if word.startswith(company):
				return company

		# for josa in JOSA:
		# 	if word.endswith(josa):
		# 		word2 = word[:-len(josa)]
		# 		if word2 in self.companyD:
		# 			return True
		return False

	def checkopinion(self, word):
		
		for opinonword,value in self.opinionD.iteritems():
			if word.startswith(opinonword):
				return opinonword,value

		return "",0

	def getcompanies(self, text):
		companycount = collections.Counter()

		for word in text.split():
			company = look.checkcompany(word)
			if company:
				companycount[company] += 1

		most = companycount.most_common()
		for w,freq in most:
		#for w,freq in companycount.iteritems():
			print w, freq

	def getopinions(self, text):
		opinioncount = collections.Counter()
		words = text.split()

		for word in words:
			opinionword,value = look.checkopinion(word)
			if value!=0:
				opinioncount[opinionword] += 1

		for i in range(len(words)-1):
			word = words[i]+" "+words[i+1]
			opinionword,value = look.checkopinion(word)
			if value!=0:
				opinioncount[opinionword] += 1


		most = opinioncount.most_common()
		for w,freq in most:
		#for w,freq in opinioncount.iteritems():
			print w, freq, self.opinionD[w]












	

def extractor(paged_url):
	#wf = codecs.open(fname, 'w', encoding='utf-8')
	paged_html = urllib2.urlopen(paged_url)
	paged_html_result = paged_html.read()
	soup = BeautifulSoup(paged_html_result,'lxml' ,from_encoding='utf-8')
	[s.extract() for s in soup('script')] # script, 자바스크립트 등 제거
	[s.extract() for s in soup('style')]	# CSS 등 style 코드 제거 
	#wf.write('%s' % soup.get_text(strip=True))
	div_list = []
	for item in soup.find_all("div"):
		item_length = len(item.get_text())
		if item_length<30 or item_length > 10000:
			continue
		item_text = item.get_text().strip().encode('utf-8')
		
		item_text = item_text.replace("\n\n","")
		item_count_n = item_text.count("\n")

		if item_count_n<5:
			item_count_n=5
		
		item_density = item_length/item_count_n


		t_item = (item_density,item_length,item_text,item_count_n)
		div_list.append(t_item)

	div_list.sort()
	div_list.reverse()


	"""
	for item in div_list:
		print "========================="
		print item[0]
		print item[1]
		print item[3]
		print item[2]
	"""




	p_list = []
	for item in soup.find_all("p"):
		item_length = len(item.get_text())
		if item_length<30 or item_length > 10000:
			continue
		item_text = item.get_text().strip().encode('utf-8')
		
		item_text = item_text.replace("\n\n","")
		item_count_n = item_text.count("\n")
		

		if item_count_n<5:
			item_count_n=5
		
		item_density = item_length/item_count_n


		t_item = (item_density,item_length,item_text,item_count_n)
		p_list.append(t_item)

	p_list.sort()
	p_list.reverse()

	"""
	for item in p_list:
		print "---------------------------========================="
		print item[0]
		print item[1]
		print item[3]
		print item[2]
	"""




	item_title = soup('title')[0]
	item_title = item_title.get_text().encode("utf-8")
	
	#print "|||||||||||||||||||||||||||||||||||||||||||||||||||"
	p_list.reverse()
	div_list.reverse()


	tmp_item = ("","","","")
	if not p_list:
		p_list.append(tmp_item)
		p_list.append(tmp_item)
	if not div_list:
		div_list.append(tmp_item)
		div_list.append(tmp_item)
		
	
	poped_div = div_list.pop()[2]
	poped_p = p_list.pop()[2]
	poped_div2 = div_list.pop()[2]
	poped_p2 = p_list.pop()[2]
	item_article = ""
	if poped_p in poped_div:
		item_article = poped_div
	elif poped_p in poped_div2:
		item_article = poped_div2
	elif poped_p2 in poped_div:
		item_article = poped_div
	elif poped_p2 in poped_div2:
		item_article = poped_div2
	else:
		item_article = poped_div	

	

	#print item_title
	#print item_article
	return item_article,item_title



if __name__=="__main__":

	#tmp = extractor("http://www.dt.co.kr/contents.html?article_no=2013091702010151747002")
	#print tmp[1]
	#print tmp[0]
	# textlist =getmaintext(trimtext(webtotext("http://news.naver.com/main/read.nhn?mode=LSD&mid=shm&sid1=101&oid=001&aid=0006525182")))
	# textlist =getmaintext(trimtext(webtotext("http://media.daum.net/economic/others/newsview?newsid=20131009090009208")))
	# textlist =getmaintext(trimtext(webtotext("http://www.ajunews.com/kor/view.jsp?newsId=20131008000076")))
	textlist =getmaintext(trimtext(webtotext("http://biz.chosun.com/site/data/html_dir/2013/10/19/2013101900924.html")))
	# textlist = getmaintext(trimtext(webtotext("http://media.daum.net/politics/north/newsview?newsid=20131009075504688")))
	print textlist;
	look = LookupCompany()
	look.loadcompanylist("kospi.txt")
	look.loadcompanylist("kosdaq.txt")
	look.loadopinionwords("positive.txt",1)
	look.loadopinionwords("negative.txt",-1)
	# print checkcompany(u"삼성전자를",D2)
	# print checkcompany(u"삼성전자와",D2)
	# print checkcompany(u"LG전자에게",D2)

	# for text in textlist:
	# 	for word in text.split():
	# 		if look.checkcompany(word):
	# 			print word
	
	look.getcompanies(textlist)
	print "----------"
	look.getopinions(textlist)



