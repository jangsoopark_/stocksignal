#-*-coding: utf-8-*-
# coding: utf-8
import re
import codecs
from bs4 import BeautifulSoup
import urllib2
import sys
import pymysql
import time
import datetime
from dateutil import parser
import re
#sys.stdout = codecs.getwriter('euc-kr')(sys.stdout)


conn = pymysql.connect(host = '127.0.0.1', port = 3306, user= 'soma', passwd = 'thak2013', database = 'stock',autocommit=True)
cur = conn.cursor()

def get_news_list(url):
	html = urllib2.urlopen(url).read()
	html_soup = BeautifulSoup(html,'lxml')
	result = []

	for link in html_soup.find_all("div","list_item"):
		link = link.find_all('a')[-1]
		title = link.string
		url = link['href']
		# print title,url
		# result.append(title+"|"+url)
		
		# result.append(dict(title = title,url = url))
		if not already_check(url):
			news = News(url,title)
			news.get_contents()
			news.insert_db()
			print news.title
			# print news.content
	# print "---------------------------"

def already_check(url):
	cur.execute("select count(*) from NEWS where news_url=%s",( url ))
	for row in cur:
		return row[0]

class News(object):
	def __init__(self,url,title):
		self.url = url
		self.title = title.encode("utf-8")
		self.content = None
		self.date = None
		self.html = urllib2.urlopen(self.url).read()
		self.soup = BeautifulSoup(self.html, 'lxml')

	def get_contents(self):
		
		self.content = self.soup.find("div", "body").get_text().strip().encode('utf-8')
		# [s.extract() for s in content('script')] # script, 자바스크립트 등 제거
		# [s.extract() for s in content('style')]	# CSS 등 style 코드 제거 
		self.get_date()

	def get_date(self):
		self.date = self.soup.find("meta",property="article:published_time")['content']
		self.date = self.date.replace("+09:00","")
		self.date = self.date.replace("T"," ")
		self.date = parser.parse(self.date)

	def to_db_escape(self):
		re.escape(self.title)
		re.escape(self.content)
	
	def insert_db(self):
		self.to_db_escape()
		try:
			cur.execute("insert into NEWS (news_url, news_title, news_content, news_date, news_crawl_date ) values ( %s, %s, %s, %s, %s )",( self.url, self.title, self.content, self.date, datetime.datetime.now()))
			# print news.title
		except:
			print "aleady or error"


get_news_list("http://www.newswire.co.kr/?md=A01&cat=100")
get_news_list("http://www.newswire.co.kr/?md=A01&cat=200")
get_news_list("http://www.newswire.co.kr/?md=A01&cat=400")
get_news_list("http://www.newswire.co.kr/?md=A01&cat=600")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=102")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=106")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=107")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=108")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=110")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=111")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=112")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=113")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=115")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=116")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=119")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=120")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=121")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=122")
get_news_list("http://www.newswire.co.kr/?md=A02&cat=123")

cur.close()
conn.close()