import sys
import urllib2
import bs4
from bs4 import BeautifulSoup

def _text_filter(text):
	if (type(text) == bs4.element.NavigableString and
		((text.parent.has_attr("class") and text.parent["class"][0] == "article") or
		(text.parent.parent.has_attr("class") and text.parent.parent["class"][0] == "article"))):
			return True

def get_news_content_from_url(url):
	html = urllib2.urlopen(url).read()
	soup = BeautifulSoup(html, "html5lib", from_encoding="utf-8")
	div = soup.find("div", class_="article")
	result = list()
	for text in div.find_all(text=_text_filter):
		if text.strip():
			result.append(text.strip().encode("utf-8"))
	return " ".join(result)

if len(sys.argv) > 1:
	url = sys.argv[1]
	print get_news_content_from_url(url)
else:
	print "Usage: python {} [news_url]".format(sys.argv[0])