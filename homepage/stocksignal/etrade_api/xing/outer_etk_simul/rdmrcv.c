/******************************************************************************
* File: rdmrcv.c
* Description: REAL 수신
* Version: 1.0
* Revisions:
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include "aslib.h"

/* LOGIN.cfg 설정파일 경로 */
#define CONFIG_PATH "/htsteam/home/example/customer/outer_etk_simul"

char 	g_HomePath[80];
char 	g_EnvPath[80];
char	g_PgmName[64];

char   *g_ConnHost;     /* Connect Host                                   */
char   *g_ConnPort;     /* Connect Port                                   */

char   *g_Dgubun;
char   *g_Keycode;

AS_HANDLE	AH;
long	prevol;

/* 압축 */
int RdmDataProcC(AS_HANDLE handle, char *data, int len)
{
	int	n;
	int	pos = 0;
	char	buff[2048], fname[1024];
	int		cnt = 0;
	long	volume, cvol;
	char	t_date[15];

	while(pos < len)
	{
		n = (uchar)data[pos] * 256;
		pos++;
		n += (uchar)data[pos];
		pos++;
		memcpy(buff, &data[pos], n);
		buff[n] = 0;
		pos += n;
		cnt++;

		printf("C[%d][%.*s]\n", cnt, strlen(buff), buff);
	}
	return 1;
}

/* 비압축 */
int RdmDataProcN(AS_HANDLE handle, char *data, int len)
{
	int	n;
	int	pos = 0;
	char	buff[2048], fname[1024];
	int		cnt = 0;
	long	volume, cvol;
	char	t_date[15];

	while(pos < len)
	{
		n = (uchar)data[pos] * 256;
		pos++;
		n += (uchar)data[pos];
		pos++;
		memcpy(buff, &data[pos], n);
		buff[n] = 0;
		pos += n;
		cnt++;

		printf("N[%d][%.*s]\n", cnt, strlen(buff), buff);
	}

	return 1;
}

int RdmMsgProc(AS_HANDLE handle)
{
	uchar 	buff[10240];
	uchar   tmp[MAX_PACKET_SIZE + 10];
	AS_COMM_PACKET *pCommPack;

    struct  timeval     timeout;
	fd_set  event, readable;
	int     n, maxfd, rc, size, loop = 1;
	struct timeval  tv;
	double atime, btime;

	pCommPack = (AS_COMM_PACKET *)buff;
	while(loop)
	{
		timeout.tv_sec = 360;     /* Second */
		timeout.tv_usec = 0;        /* micro Second */
		FD_ZERO(&event);
		FD_SET(handle->m_rfd, &event);
		maxfd = handle->m_rfd + 1;

		rc = select(maxfd, &event, (fd_set *)0,(fd_set *)0,
									(struct timeval *) &timeout);
		if(rc > 0)
		{
			if(FD_ISSET(handle->m_rfd, &event))
			{
				if(AsCheckFd(handle) <= 0)
				{
					return AS_STOP;
				}
				while(1) 
				{
					n = AsRecvPacket(handle, buff);
					if( n <= 0) break;
					buff[n] = 0;
					size = n - sizeof(AS_COMM_HEAD);
					if(pCommPack->head.cmd[0] == AS_SISE_DATA)
					{
						if (pCommPack->head.flag[0] & AS_COMPRESS)
						{
							size = DecompressLZO(pCommPack->data,tmp, size);
							memcpy((char *)pCommPack->data, tmp, size);
						    RdmDataProcC(handle, (char *)pCommPack->data, size);
						}
						else
						    RdmDataProcN(handle, (char *)pCommPack->data, size);

					}
				}
			}
		}
		else if(rc == 0)
		{
			printf("Timeout................\n");
		}
		else
		{
			printf("signal no=%d...............\n", errno);
		}
	}
	return 1;
}

int main(int argc, char *argv[])
{
	int rc;
	char *as_home;
	char logname[256];
	char temp[10], code[10+1]={0,};
	char 	id[16], pwd[16], acc[16], accpwd[16], branch[16];

    as_home = getenv("ASHOME");
	if (as_home != NULL)
		strcpy(g_HomePath, as_home);
	else
		strcpy(g_HomePath, "..");

	sprintf(g_EnvPath, "%s/env", g_HomePath);

	if(argc != 3) {
		printf("Usage : %s [DataGubun] [KeyCode]\n",
			argv[0]);
        printf("        DataGubun - S3_,H1_,IJ_\n");
        printf("        KeyCode - 005930,000660,...\n");
		exit(0);
	}

	g_Dgubun = (char *)argv[1];
	g_Keycode = (char *)argv[2];

	AH = AsInitial(0, NULL);
	if (AH == NULL)
	{
		printf("AS_HANDLE initial Error\n");
		exit(0);
	}

    /*=============================================================================*/
    /* 고객 환경 Load 및 로그인 - return값 확인하세요.                             */
    /*-----------------------------------------------------------------------------*/
    /*  rc = -3 : 공인인증실패                                                     */
    /*  rc = -2 : 로그인실패                                                       */
    /*  rc = -1 : 서버접속실패                                                     */
    /*  rc =  0 : TR전송실패                                                       */
    /*  rc =  1 : 성공                                                             */
    /*-----------------------------------------------------------------------------*/
    rc = API_LOGIN_SIMUL(AH,  CONFIG_PATH, "LOGIN.cfg");
    if(rc <= 0)
    {
        printf("rc:%d [Error return]\n", rc);
        exit(0);
    }
    /*=============================================================================*/

	/* 환경파일에서 변수 읽어오기 */
	/* 변수는 추가로 지정해서 사용가능합니다. 아래는 기본항목 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "ID", "",  id);	  /* ID		  */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "PW", "",  pwd);	  /* 패스워드 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "AC", "",  acc);	  /* 계좌번호 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "AP", "",  accpwd);/* 계좌비번 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "BC", "",  branch);/* 지점번호 */

	/* 기본설정값 보기 */
	printf("아이디  : [%s]\n", id);
	printf("패스원드: [%s]\n", pwd);
	printf("계좌번호: [%s]\n", acc);
	printf("계좌비번: [%s]\n", accpwd);
	printf("지점번호: [%s]\n", branch); /* 담당자에게 확인후 넣으세요. */

	rc = AsAdviseSise(AH, g_Dgubun, g_Keycode);

	RdmMsgProc(AH);

	AsTerminate(AH);

	return 0;
}
