#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <errno.h>
#include "FutOrd.h"
#include "aslib.h"

#define MAX_BUFF_LEN   4096
extern int errno;

AS_HANDLE   AH;

#define SERVIP "61.106.5.58"
//#define CONFIG_PATH "/home/trbsysre/TEST/src/example"
#define CONFIG_PATH "/htsteam/home/example/customer/outer_etk_simul"

int main(int argc, char *argv[])
{
	int wlen = 0, rlen = 0, qty, rc, roklen=0, occcnt, ii, slen;
	float	price;
	char *addr, *port, *shcode, sbuff[4096+1], rbuff[40960+1], tmp[48], logname[1024];
	SB_NHAP	sbhd;
	CCEBQ10500InBlock1 InBlock1;
	CCEBQ10500OutBlock1 OutBlock1;
	CCEBQ10500OutBlock2 *OutBlock2;
	CCEBQ10500OutBlock3 *OutBlock3;
    char      encode[1024], decode[4096];

    AH = AsInitial(0, NULL);
    if (AH == NULL)
    {
        printf("AS_HANDLE initial Error\n");
        exit(0);
    }

    /* =============================================== */
    /* 고객 환경 Load 및 로그인 - plase, check it out  */
    /* =============================================== */
	/*  rc = -3 : 공인인증실패                         */
	/*  rc = -2 : 로그인실패                           */
	/*  rc = -1 : 서버접속실패                         */
	/*  rc =  0 : TR전송실패                           */
	/*  rc =  1 : 성공                                 */
    rc = API_LOGIN_SIMUL(AH,  CONFIG_PATH, "LOGIN.cfg");
	if(rc <= 0)
	{
		printf("rc:%d [Error return]\n", rc);
		exit(0);
	}

    memset((char *)&sbhd, 0x20, sizeof(sbhd));
    memset((char *)&InBlock1, 0x20, sizeof(InBlock1));
    memset(sbuff, 0x00, sizeof(sbuff));
    memset(rbuff, 0x00, sizeof(rbuff));

    /* =============================================== */
    /* 헤더 고객 입력영역 please check it out          */
    /* =============================================== */
    memcpy(sbhd.UserID, "h2o01    ", sizeof(sbhd.UserID)); /* ID check */
    memcpy(sbhd.IPAddr, "222160022160    ", sizeof(sbhd.IPAddr)); /*** IP  check ***/
    memcpy(sbhd.BranchNo, "004", sizeof(sbhd.BranchNo)); /*** 관리점 check ***/
    /* =============================================== */


    memcpy(sbhd.MediaETK, "s ", sizeof(sbhd.MediaETK)); /* API당사매체 */
    memcpy(sbhd.ServiceCode, "CCEBQ10500", sizeof(sbhd.ServiceCode)); /* 서비스코드 check */
	sbhd.FuncCode[0] = 'C';
	sbhd.Cont[0] = 'N';
	sbhd.Lang[0] = 'K';
	sbhd.Loan[0] = '0';
    memcpy(sbhd.MediaComm, "41", sizeof(sbhd.MediaComm)); /* API매체 */
	sbhd.BussGb[0] = '0';

    /* =============================================== */
    /* 전문 고객 입력영역 please check it out          */
    /* =============================================== */
    memcpy(InBlock1.RecCnt, "00001", sizeof(InBlock1.RecCnt));/* 갯수check */
    memcpy(InBlock1.AcntNo, "00451707274         ", sizeof(InBlock1.AcntNo));/* 계좌check*/
    memcpy(InBlock1.Pwd, "0709    ", sizeof(InBlock1.Pwd)); /* 패스워드check */

    memcpy(sbuff, &sbhd, sizeof(sbhd));
    memcpy(sbuff+sizeof(sbhd), &InBlock1, sizeof(InBlock1));

	/* 암호화 */
    slen = AsEncode(sbuff, sizeof(SB_NHAP)+sizeof(InBlock1), encode);
	rc = AsCallTran(AH, "a7710", encode, slen, rbuff, 5);

    /* 복호화 */
    rlen = AsDecode(rbuff,decode,rc);

    OutBlock2 = (CCEBQ10500OutBlock2 *)&decode[sizeof(CCEBQ10500OutBlock1)];
	printf("예탁금총액 예수금 증거금액\n");
	printf("%.16s:%.16s:%.16s\n", OutBlock2->DpsamtTotamt, OutBlock2->Dps, OutBlock2->Mgn);
	memset(tmp, 0x20, sizeof(tmp));
	sprintf(tmp, "%5.5s", &decode[sizeof(CCEBQ10500OutBlock1)+sizeof(CCEBQ10500OutBlock2)]);
	occcnt = atoi(tmp);
	printf("rlen:[%d:%d] occcnt:%d\n", rlen, strlen(decode), occcnt);
	
	for(ii = 0; ii < occcnt; ii++)
	{
		OutBlock3 = (CCEBQ10500OutBlock3 *)&decode[sizeof(CCEBQ10500OutBlock1)+sizeof(CCEBQ10500OutBlock2)+5+sizeof(CCEBQ10500OutBlock3)*ii];
		printf("%.20s:%.16s:%.16s:%.16s\n", OutBlock3->PdGrpCodeNm, OutBlock3->NetRiskMgn, OutBlock3->PrcMgn, OutBlock3->PrcFlctMgn);
	}
	AsTerminate(AH);
	return 0;
}
