#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <errno.h>
#include "FutOrd.h"
#include "aslib.h"

#define MAX_BUFF_LEN   4096
extern int errno;

AS_HANDLE   AH;

#define SERVIP "61.106.5.53" /* check */
#define CONFIG_PATH "/htsteam/home/example/customer/outer_etk_simul"

int main(int argc, char *argv[])
{
	int rlen = 0, slen = 0, rc, roklen, qty;
	char *shcode, sbuff[4096+1], rbuff[4096+1], tmp[48], logname[1024];
	char ip[16], port[16], id[16], pwd[16], acc[16], accpwd[16];
	char juno[16], dnname[128], dnfile[128], logpath[64], branch[16];
	float	price;
	SB_NHAP	sbhd;
    CCEAT00100InBlock1 InBlock1;
    CCEAT00100OutBlock1 OutBlock1;
    CCEAT00100OutBlock2 *OutBlock2;
    char      encode[1024], decode[4096];
	char	  Temp[1024];

	if(argc != 3)
	{
		printf("usage : a1301 주문가격 주문수량\n");
        printf("    종목,가격은 샘플소스내의 check 항목을 확인하세요.\n");
		return 0;
	}

    AH = AsInitial(0, NULL);
    if (AH == NULL)
    {
        printf("AS_HANDLE initial Error\n");
        exit(0);
    }

	price = atof(argv[1]);
	qty = atoi(argv[2]);

	/* 환경파일에서 변수 읽어오기 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "IP", "",  ip);  	/* IP	 	*/
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "PT", "",  port);	/* PORT 	*/
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "ID", "",  id);		/* ID		*/
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "PW", "",  pwd);		/* 패스워드 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "AC", "",  acc);		/* 계좌번호 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "JM", "",  juno);	/* 주민번호 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "DN", "",  dnname);  /* 인증서명 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "SG", "",  dnfile);  /* 인증파일 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "LG", "",  logpath); /* 로그경로 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "BC", "",  branch);  /* 지점번호 */
    GetProfileStr( CONFIG_PATH, "LOGIN.cfg", "LOGIN",  "AP", "",  accpwd);  /* 계좌비번 */

	printf("IP      : [%s]\n", ip);
	printf("PORT    : [%s]\n", port);
	printf("ID      : [%s]\n", id);
	printf("PASSWD  : [%s]\n", pwd);
	printf("ACCOUNT : [%s]\n", acc);
	printf("JUMINNO : [%s]\n", juno);
	printf("SIGNNAME: [%s]\n", dnname);
	printf("SIGNFILE: [%s]\n", dnfile);
	printf("LOGPATH : [%s]\n", logpath);
	printf("BRANCH  : [%s]\n", branch);

    /* =============================================== */
    /* 고객 환경 Load 및 로그인 - plase, check it out  */
    /* =============================================== */
	/*  rc = -3 : 공인인증실패                         */
	/*  rc = -2 : 로그인실패                           */
	/*  rc = -1 : 서버접속실패                         */
	/*  rc =  0 : TR전송실패                           */
	/*  rc =  1 : 성공                                 */
    rc = API_LOGIN_SIMUL(AH,  CONFIG_PATH, "LOGIN.cfg");
	if(rc <= 0)
	{
		printf("rc:%d [Error return]\n", rc);
		exit(0);
	}

    memset((char *)&sbhd, 0x20, sizeof(sbhd));
    memset((char *)&InBlock1, 0x20, sizeof(InBlock1));
    memset(sbuff, 0x00, sizeof(sbuff));
    memset(rbuff, 0x00, sizeof(rbuff));
    memset(decode, 0x00, sizeof(decode));

    /* =============================================== */
    /* 헤더 고객 입력영역 please check it out          */
    /* =============================================== */
    memcpy(sbhd.UserID, id, strlen(id)); /*** ID check ***/
    memcpy(sbhd.IPAddr, ip, strlen(ip)); /*** IP  check ***/
    memcpy(sbhd.BranchNo, branch, strlen(branch)); /*** 관리점 check ***/
    /* =============================================== */

    memcpy(sbhd.MediaETK, "s ", sizeof(sbhd.MediaETK)); /* API당사매체 */
    memcpy(sbhd.ServiceCode, "CCEAT00100", sizeof(sbhd.ServiceCode));
	sbhd.FuncCode[0] = 'C';
	sbhd.Cont[0] = 'N';
	sbhd.Lang[0] = 'K';
	sbhd.Loan[0] = '0';
    memcpy(sbhd.MediaComm, "41", sizeof(sbhd.MediaComm)); /* API매체 */
	sbhd.BussGb[0] = '0';


    /* =============================================== */
    /* 전문 고객 입력영역 please check it out          */
    /* =============================================== */
    memcpy(InBlock1.AcntNo, acc, strlen(acc));/* 계좌check */
    memcpy(InBlock1.Pwd, accpwd, strlen(accpwd));/* 비밀번호 check */
    memcpy(InBlock1.FnoIsuNo, "101H9000    ", sizeof(InBlock1.FnoIsuNo));/* 종목check */
    InBlock1.BnsTpCode[0] = '2'; /* 1:매도 2:매수 check */
    /* 00:지정가 03:시장가 10:지정가(IOC) 13:시장가(IOC) check */
    memcpy(InBlock1.FnoOrdprcPtnCode, "00", sizeof(InBlock1.FnoOrdprcPtnCode));
	sprintf(tmp, "%015.2f", price); /* 가격check */
    memcpy(InBlock1.OrdPrc, tmp , sizeof(InBlock1.OrdPrc));/* 주문가격 check */
	sprintf(tmp, "%016d", qty);
    memcpy(InBlock1.OrdQty, tmp, sizeof(InBlock1.OrdQty));/* 수량check */
    /* =============================================== */

    memcpy(InBlock1.RecCnt, "00001", sizeof(InBlock1.RecCnt));
    memcpy(InBlock1.FnoOrdPtnCode, "00", sizeof(InBlock1.FnoOrdPtnCode)); /*유형 check*/
    memcpy(InBlock1.FnoTrdPtnCode, "03", sizeof(InBlock1.FnoTrdPtnCode));
    memcpy(InBlock1.OrdMktCode, "40", sizeof(InBlock1.OrdMktCode));
    memcpy(InBlock1.CommdaCode, "41", sizeof(InBlock1.CommdaCode)); 
    memcpy(InBlock1.DscusBnsCmpltTime, "         ", sizeof(InBlock1.DscusBnsCmpltTime));
    memcpy(InBlock1.GrpId, "                    ", sizeof(InBlock1.GrpId));
    memcpy(InBlock1.OrdSeqno, "0000000000", sizeof(InBlock1.OrdSeqno));
    memcpy(InBlock1.PtflNo, "0000000000", sizeof(InBlock1.PtflNo));
    memcpy(InBlock1.BskNo, "0000000000", sizeof(InBlock1.BskNo));
    memcpy(InBlock1.TrchNo, "0000000000", sizeof(InBlock1.TrchNo));
    memcpy(InBlock1.ItemNo, "0000000000000000", sizeof(InBlock1.ItemNo));
    memcpy(InBlock1.OpDrtnNo, "            ", sizeof(InBlock1.OpDrtnNo));
    memcpy(InBlock1.MgempNo, "         ", sizeof(InBlock1.MgempNo));
    memcpy(InBlock1.FundId, "0           ", sizeof(InBlock1.FundId));
    memcpy(InBlock1.FundOrdNo, "0000000000", sizeof(InBlock1.FundOrdNo));

    memcpy(sbuff, &sbhd, sizeof(sbhd));
    memcpy(sbuff+sizeof(sbhd), &InBlock1, sizeof(InBlock1));

	/* 암호화 */
    slen = AsEncode(sbuff, sizeof(SB_NHAP)+sizeof(InBlock1), encode);
    printf(" !@ sbuff [%d][%s]\n\n", sizeof(SB_NHAP)+sizeof(InBlock1), sbuff);
    printf(" [%d][%s]\n", slen, encode);

printf(" !@#!@# AsEncode() slen[%d], strlen(encode)[%d] \n", slen, strlen(encode)); 

	rc = AsCallTran(AH, "a7810", encode, slen, rbuff, 5);
    printf("!@# AsCallTran rtn[%d]\n", rc);

	/* 복호화 */
    rlen = AsDecode(rbuff,decode,rc);
printf(" !@#!@# AsDecode() rlen[%d], strlen(decode)[%d] \n", rlen, strlen(decode)); 

	printf("[%d][%d][%s]\n", rlen, strlen(decode), decode);

    OutBlock2 = (CCEAT00100OutBlock2 *)&decode[sizeof(CCEAT00100OutBlock1)];

	printf("주문접수번호[%.*s]\n", sizeof(OutBlock2->OrdNo), OutBlock2->OrdNo);
    printf("[%.5s] %s\n", AH->m_MsgCode,  AH->m_MsgStr);

    AsTerminate(AH);

	return 0;
}
