#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <errno.h>
#include "FutOrd.h"
#include "aslib.h"

#define MAX_BUFF_LEN   4096
extern int errno;

AS_HANDLE   AH;

#define SERVIP "61.106.5.58" /* check */
#define CONFIG_PATH "/home/trbsysre/TEST/src/example"

int main(int argc, char *argv[])
{
	int rlen = 0, slen = 0, rc, roklen, qty;
	char *shcode, sbuff[4096+1], rbuff[4096+1], tmp[48], logname[1024];
	float	price;
	SB_NHAP	sbhd;
    CFOAT00100InBlock1 InBlock1;
    CFOAT00100OutBlock1 OutBlock1;
    CFOAT00100OutBlock2 *OutBlock2;
    char      encode[1024], decode[4096];

	if(argc != 3)
	{
		printf("usage : a1301 주문가격 주문수량\n");
        printf("    종목,가격은 샘플소스내의 check 항목을 확인하세요.\n");
		return 0;
	}

    AH = AsInitial(0, NULL);
    if (AH == NULL)
    {
        printf("AS_HANDLE initial Error\n");
        exit(0);
    }

	price = atof(argv[1]);
	qty = atoi(argv[2]);
    /* =============================================== */
    /* 고객 입력영역 - plase, check it out             */
    /* =============================================== */
    rc = AsConnect(AH, SERVIP, "20001", 0); /* IP check */
    if (rc == 0)
    {
        printf("AsCH Connect Error\n");
        exit(0);
    }

    memset((char *)&sbhd, 0x20, sizeof(sbhd));
    memset((char *)&InBlock1, 0x20, sizeof(InBlock1));
    memset(sbuff, 0x00, sizeof(sbuff));
    memset(rbuff, 0x00, sizeof(rbuff));
    memset(decode, 0x00, sizeof(decode));

    /* =============================================== */
    /* 헤더 고객 입력영역 please check it out          */
    /* =============================================== */
    memcpy(sbhd.UserID, "jdh1234 ", sizeof(sbhd.UserID)); /*** ID check ***/
    memcpy(sbhd.IPAddr, "192168022160    ", sizeof(sbhd.IPAddr)); /*** IP  check ***/
    memcpy(sbhd.BranchNo, "100", sizeof(sbhd.BranchNo)); /*** 관리점 check ***/
    /* =============================================== */

    memcpy(sbhd.MediaETK, "s ", sizeof(sbhd.MediaETK)); /* API당사매체 */
    memcpy(sbhd.ServiceCode, "CFOAT00100", sizeof(sbhd.ServiceCode));
	sbhd.FuncCode[0] = 'C';
	sbhd.Cont[0] = 'N';
	sbhd.Lang[0] = 'K';
	sbhd.Loan[0] = '0';
    memcpy(sbhd.MediaComm, "41", sizeof(sbhd.MediaComm)); /* API매체 */
	sbhd.BussGb[0] = '0';


    /* =============================================== */
    /* 전문 고객 입력영역 please check it out          */
    /* =============================================== */
    memcpy(InBlock1.AcntNo, "20007328103         ", sizeof(InBlock1.AcntNo));/* 계좌check */
    memcpy(InBlock1.Pwd, "0709    ", sizeof(InBlock1.Pwd));/* 비밀번호 check */
    memcpy(InBlock1.FnoIsuNo, "301H6247    ", sizeof(InBlock1.FnoIsuNo));/* 종목check */
    InBlock1.BnsTpCode[0] = '2'; /* 1:매도 2:매수 check */
    /* 00:지정가 03:시장가 10:지정가(IOC) 13:시장가(IOC) check */
    memcpy(InBlock1.FnoOrdprcPtnCode, "00", sizeof(InBlock1.FnoOrdprcPtnCode));
	sprintf(tmp, "%015.2f", price); /* 가격check */
    memcpy(InBlock1.OrdPrc, tmp , sizeof(InBlock1.OrdPrc));/* 주문가격 check */
	sprintf(tmp, "%016d", qty);
    memcpy(InBlock1.OrdQty, tmp, sizeof(InBlock1.OrdQty));/* 수량check */
    /* =============================================== */

    memcpy(InBlock1.RecCnt, "00001", sizeof(InBlock1.RecCnt));
    memcpy(InBlock1.FnoOrdPtnCode, "00", sizeof(InBlock1.FnoOrdPtnCode)); /*유형 check*/
    memcpy(InBlock1.FnoTrdPtnCode, "03", sizeof(InBlock1.FnoTrdPtnCode));
    memcpy(InBlock1.OrdMktCode, "40", sizeof(InBlock1.OrdMktCode));
    memcpy(InBlock1.CommdaCode, "41", sizeof(InBlock1.CommdaCode)); 
    memcpy(InBlock1.DscusBnsCmpltTime, "         ", sizeof(InBlock1.DscusBnsCmpltTime));
    memcpy(InBlock1.GrpId, "                    ", sizeof(InBlock1.GrpId));
    memcpy(InBlock1.OrdSeqno, "0000000000", sizeof(InBlock1.OrdSeqno));
    memcpy(InBlock1.PtflNo, "0000000000", sizeof(InBlock1.PtflNo));
    memcpy(InBlock1.BskNo, "0000000000", sizeof(InBlock1.BskNo));
    memcpy(InBlock1.TrchNo, "0000000000", sizeof(InBlock1.TrchNo));
    memcpy(InBlock1.ItemNo, "0000000000000000", sizeof(InBlock1.ItemNo));
    memcpy(InBlock1.OpDrtnNo, "            ", sizeof(InBlock1.OpDrtnNo));
    memcpy(InBlock1.MgempNo, "         ", sizeof(InBlock1.MgempNo));
    memcpy(InBlock1.FundId, "0           ", sizeof(InBlock1.FundId));
    memcpy(InBlock1.FundOrdNo, "0000000000", sizeof(InBlock1.FundOrdNo));

    memcpy(sbuff, &sbhd, sizeof(sbhd));
    memcpy(sbuff+sizeof(sbhd), &InBlock1, sizeof(InBlock1));

	/* 암호화 */
    slen = AsEncode(sbuff, sizeof(SB_NHAP)+sizeof(InBlock1), encode);

	rc = AsCallTran2(AH, "a1301", encode, slen, rbuff, 5);

	/* 복호화 */
    rlen = AsDecode(rbuff,decode,rc);
    OutBlock2 = (CFOAT00100OutBlock2 *)&decode[sizeof(SB_NHAP)+sizeof(CFOAT00100OutBlock1)];

    /* 수신패킷 뒤에 메세제80byte 추가되어 내려옴) */
    /* 수신패킷기본크기 : roklen , 추가로 메세지80byte가 내려옴 */
    roklen = sizeof(SB_NHAP)+sizeof(CFOAT00100OutBlock1)+sizeof(CFOAT00100OutBlock2);

    /* 수신패킷기본size보다 작으면 아래와 같이 메세지확인 */
    if(rlen < roklen)
    {
        printf("rlen:%d roklen:%d [%s]\n", rlen, roklen, &decode[sizeof(SB_NHAP)]);
    }
    else
    {
        printf("rlen:%d roklen:%d [%s]\n", rlen, roklen, &decode[roklen]);
        printf("주문접수번호[%.*s][%.80s]\n", sizeof(OutBlock2->OrdNo), OutBlock2->OrdNo,
        &decode[sizeof(SB_NHAP)+sizeof(CFOAT00100OutBlock1)+sizeof(CFOAT00100OutBlock2)]);
    }

    AsTerminate(AH);

	return 0;
}
