/**********************************************************************
** Version          : 2.0.0
** 프로그램  이름   : As Platform include
** 소스 화일 이름   : customer.h
** 마지막 작성일    : 2013. 1. 22
** 작 성 자         : wimble
**********************************************************************/
#ifndef __CUSTOMER_H__
#define __CUSTOMER_H__

#ifdef  __cplusplus
extern "C" {
#endif

typedef struct  _NAHAP {
    char conti               [1]; /* 연속구분                       		*/
                              /* s->c 0(None),1(Next),2(Prev),3(Next,Prev)  */
                              /* c->s 0(AOAE),1(Next),2(Prev)               */
    char trsrc               [1]; /* 주문 또는 조회발원지 					*/
    char filler              [8]; /* filler                                 */
} NAHAP;

typedef struct  _NAHAP2 {
    char conti               [1]; /* 연속구분                       		*/
                              /* s->c 0(None),1(Next),2(Prev),3(Next,Prev)  */
                              /* c->s 0(AOAE),1(Next),2(Prev)               */
    char trsrc               [1]; /* 주문 또는 조회발원지 					*/
    char userid              [8]; /* userid                                 */
    char trcode              [5]; /* trcode                                 */
} NAHAP2;

typedef struct  _TUXAP {     
    char conti              [ 1]; /* 연속구분								*/
                              /* s->c 0(None),1(Next),2(Prev),3(Next,Prev)   */
                              /* c->s 0(AOAE),1(Next),2(Prev)                */
    char tuxcode            [ 5]; 

    char media              [ 1]; 
    char sessionkey         [ 6]; 
    char screenno           [ 4]; 
    char sendseqno          [ 7]; 
    char loan               [ 1]; 
    char userid             [ 8]; 
    char ipaddr             [12]; 
    char funcode            [ 1]; 
    char msgcode            [ 4]; 
    char msg                [49]; 
    char filler             [ 1]; 
} TUXAP;  

typedef struct  _SB_NHAP
{
    char MediaETK          [2]; /* 당사매체                                */
    char SessionKey        [6]; /* 세션키                                  */
    char ScreenNo          [5]; /* 화면번호                                */
    char SendSeqNo         [7]; /* 전송TR SeqNo                            */
    char UserID            [8]; /* Login ID                                */
    char IPAddr           [16]; /* IP Address  192004015023                */
    char ServiceCode      [10]; /* Service Code                            */
    char BranchNo          [3]; /* 관리점                                  */
    char FuncCode          [4]; /* 처리구분  1:등록,2:조회,3:정정,4:취소,  */
                                /*         5:확인,6:전화면,7:연속,C:전송   */
    char Cont              [1]; /* 연속구분  Y:연속,N:NONE                 */
    char ContKey          [18]; /* 연속키                                  */
    char Lang              [1]; /* 언어 K:한국,E:영어,C:중국,J:일본        */
    char RqCnt             [4]; /* 요청건수                                */
    char Loan              [1]; /* 론구분 1:파워론,0:사용안함              */
    char MediaComm         [2]; /* 통신매체                                */
    char BussGb            [1]; /* 업무헤서추가여부 1:업무헤더있음,0:NONE  */
    char Filler            [9]; /* Filler                                  */
    char UserData         [26]; /* 유저영역                                */
} SB_NHAP;

int	TrenuSetAHeader(NAHAP *aheader, int cont);
int AsEncode(char *text, int numBytes, char *encodedText);
int AsDecode(char *text, char *dst, int numBytes);

#ifdef  __cplusplus
}
#endif

#endif
