# -*- coding: utf-8 -*-
import urllib
import urllib2
from bs4 import BeautifulSoup
import pymysql
import time
import datetime
from dateutil import parser

conn = pymysql.connect(host = '127.0.0.1', port = 3306, user= 'soma', passwd = 'thak2013', database = 'stock',autocommit=True)
cur = conn.cursor()

response = urllib2.urlopen("http://stock.koscom.co.kr/news/news_time_list.jsp")
soup = BeautifulSoup(response.read())


nid = 0
for ul in soup.find_all("tr", "table_data"):
	result = dict(all = ul.a["href"].encode("utf-8"),
				  title = ul.a.string.encode("utf-8"),
				  date = ul.find_all("td")[2].get_text() + " " + ul.find_all("td")[1].get_text())
	text = result["all"]
	texturl = text.split('?')[1]
	texturl = texturl.split('\'')[0]
	texturl = "http://stock.koscom.co.kr/news/news_content.jsp?"+texturl


	response2 = urllib2.urlopen(texturl)
	soup2 = BeautifulSoup(response2.read())


	for ul in soup2.find_all("div"):
		content = ul.pre.get_text().encode("utf-8")

		print nid, result["date"], result["title"], texturl, "\n", content
		nid = nid + 1
		#cur.execute("insert into NEWS (news_title, news_url, news_content, news_date, news_crawl_date ) values ( %s, %s, %s, %s, %s )",( result["title"], texturl, content, parser.parse(result["date"]), datetime.datetime.now()))
		try:
			cur.execute("replace into NEWS (news_url, news_title, news_content, news_date, news_crawl_date ) values ( %s, %s, %s, %s, %s )",( texturl, result["title"], content, parser.parse(result["date"]), datetime.datetime.now()))
		except:
			print "error"


cur.close()
conn.close()
