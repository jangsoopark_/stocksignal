# -*- coding: utf-8 -*-
import urllib
import urllib2
from bs4 import BeautifulSoup
import pymysql
import time
import datetime
from dateutil import parser
import re

conn = pymysql.connect(host = '211.110.140.229', port = 3306, user= 'soma', passwd = 'thak2013', database = 'stock',autocommit=True)
cur = conn.cursor()


response = urllib2.urlopen("http://news.naver.com/main/list.nhn?mode=LSD&mid=sec&sid1=101&listType=title")
soup = BeautifulSoup(response.read())

results = list()

def webtotext(paged_url):
	#wf = codecs.open(fname, 'w', encoding='utf-8')
	paged_html = urllib2.urlopen(paged_url)
	paged_html_result = paged_html.read()
	soup = BeautifulSoup(paged_html_result, 'lxml' ,from_encoding='utf-8')
	[s.extract() for s in soup('script')] # script, 자바스크립트 등 제거
	[s.extract() for s in soup('style')]	# CSS 등 style 코드 제거 
	return soup.get_text()

def trimtext(text):
	text = text.replace("\r\n","\n")
	return text.replace("\n\n","\n")

def getmaintext(text):
	lines = text.splitlines()
	# lines = sorted(lines,key= lambda line:len(line),reverse=True)
	lines = filter(lambda line: len(line)>100, lines)
	return "\n".join(lines)

def already_check(url):
	cur.execute("select count(*) from NEWS where news_url=%s",( url ))
	for row in cur:
		return row[0]


for ul in soup.find("div", "list_body newsflash_body").find_all("ul", "type02"):
	for li in ul.find_all("li"):
		result = dict(url = li.a["href"], 
					  title = li.a.string.encode("utf-8"),
					  date = li.find("span", "date").string)
		
		results.append(result)
		
		if not already_check(result["url"]):
			print result["title"], result["url"], result["date"]
			print "----------------------------------------"
			text = getmaintext(trimtext(webtotext(result["url"])))
			text = text.encode("utf-8")
			print text
			print "======================================="

			news_data = parser.parse(result["date"])
			try:
				cur.execute("""insert into NEWS (news_url, news_title, news_content, news_date, news_crawl_date ) values ('{}', '{}', '{}', '{}', '{}')""".format( result["url"], re.escape(result["title"]), re.escape(text), news_data, datetime.datetime.now()))
			except:
				print "aleady or error"

			time.sleep(1)



	

cur.close()
conn.close()