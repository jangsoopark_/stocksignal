# coding: utf-8

import sys
import pymysql

def encode(string, encoding="utf-8"):
	if isinstance(string, str):
		return string
	elif isinstance(string, unicode):
		return string.encode(encoding)

	return string

def decode(string, encoding="utf-8"):
	if isinstance(string, str):
		return string.decode(encoding)
	elif isinstance(string, unicode):
		return string

	return string # 위의 두 가지 경우가 아닐 때. 하지만 이런 경우는 없다고 보는게 편하겠지?

def colored(content, color="yellow"):
	colors = {"red": 91, "blue": 94, "green": 92, "yellow": 93, "pink": 95}

	if color in colors:
		return "\033[{}m{}\033[0m".format(colors[color], content)
	else:
		return colored(content, "yellow")

class OpMi_Base(object):
	pass
	# def _find_words(self, ... # 추가 하자. 귀찮다. ... 하려고 했으나 형식이 미묘하게 달라서 일단 보류. 역시 귀찮아

class OpMi_DataManager(OpMi_Base):
	def _load_data_from_file(self, file_name, line_processor, data):
		try:
			file = open(file_name, "rt")
		except IOError:
			print "error: cannot open file %s" % (file_name)

			return False
		# data = [] # 이렇게 하면 작동이 안 되는 것 같아서 주석 처리해둠.

		for line in file:
			stripped_line = line.strip()
			if stripped_line:
				line_processor(stripped_line, data)

		file.close()

		return True

class OpMi_OpinionFinder(OpMi_DataManager):
	def __init__(self, positive_words_file_name=None, negative_words_file_name=None):
		result = True

		self.positive_words = []
		self.negative_words = []

		if positive_words_file_name:
			result &= self._load_data_from_file(positive_words_file_name, self._line_processor, self.positive_words)
		if negative_words_file_name:
			result &= self._load_data_from_file(negative_words_file_name, self._line_processor, self.negative_words)

		if result is False:
			print colored("an error occurred while loading data from files.", "red")

	def feed(self, content):
		result = [{}, {}]

		result[0] = self._find_opinion_words(content, self.positive_words)
		result[1] = self._find_opinion_words(content, self.negative_words)

		return result

	def _find_opinion_words(self, content, opinion_words):
		result = {}

		content = decode(content)

		for opinion_word in opinion_words:
			for possible_combination in self._possible_combinations(opinion_word):
				possible_combination = decode(possible_combination)
				length = len(possible_combination)

				next_content = content

				while True:
					index = next_content.find(possible_combination)
					if index >= 0:
						if opinion_word in result:
							result[opinion_word][0] += 1
							result[opinion_word][1].append(index)
						else:
							result[opinion_word] = [1, [index]]

						next_content = next_content[index + length:]
					else:
						break

		return result

	def _combine_words(self, words, start_index, count):
		result = []

		length = len(words)

		if isinstance(words, (tuple, list)):
			temp = ""

			i = 0
			while i < length:
				if i >= start_index and i < start_index + count:
					temp += words[i]
				else:
					if temp is not "":
						result.append(temp)

						temp = ""

						i -= 1
					else:
						result.append(words[i])
				i += 1
			if temp is not "":
				result.append(temp)

		return " ".join(result)

	def _possible_combinations(self, combined_word):
		result = []

		if isinstance(combined_word, str):
			words = combined_word.split()
			length = len(words)

			result.append(self._combine_words(words, 0, 0))

			for i in range(2, length + 1):
				for j in range(0, length - i + 1):
					result.append(self._combine_words(words, j, i))

		return result

	def _line_processor(self, line, data):
		data.append(line)

class OpMi_CompanyFinder(OpMi_DataManager):
	def __init__(self, companies_file_name=None):
		result = True

		self.companies = []

		if companies_file_name:
			result &= self._load_data_from_file(companies_file_name, self._line_processor, self.companies)

		if result is False:
			print colored("an error occurred while loading data from files.", "red")

	def feed(self, content):
		result = self._find_companies(content)

		return result

	def _find_companies(self, content):
		result = {}

		content = decode(content)

		for (company, company_id) in self.companies:
			company = decode(company)
			length = len(company)

			next_content = content

			while True:
				index = next_content.find(company)
				if index >= 0:
					if encode(company) in result:
						result[encode(company)][1] += 1
						result[encode(company)][2].append(index)
					else:
						result[encode(company)] = [int(company_id), 1, [index]]

					next_content = next_content[index + length:]
				else:
					break

		return result

	def _line_processor(self, line, data):
		if not line.startswith("#"):
			index = line.find("|")
			if index >= 0:
				company_id = line[:index]
				company = line[index + 1:]

				data.append((company, company_id))

def sort_key(x):
	return x[2]

def get_opinion_score(op_finder, content, positive_multiple=1, negative_multiple=1):
	positive_score = 0
	negative_score = 0

	result = op_finder.feed(content)

	for (positive_word, data) in result[0].iteritems():
		positive_score += data[0] * positive_multiple

	for (negative_word, data) in result[1].iteritems():
		negative_score += data[0] * negative_multiple

	return (positive_score, negative_score)

def get_most_frequent_company(cp_finder, content, frequency_multiple=1):
	result = cp_finder.feed(content)

	sorted_result = sorted([(company, data[0], data[1]) for (company, data) in result.iteritems()], key=sort_key, reverse=True)

	if sorted_result:
		return (sorted_result[0][0], sorted_result[0][1], sorted_result[0][2] * frequency_multiple)
	else:
		return (None, None, None)

def db_query(conn, query):
	cur = conn.cursor()
	cur.execute(query)
	cur.close()

op_finder = OpMi_OpinionFinder("../positive_words.txt", "../negative_words.txt")
cp_finder = OpMi_CompanyFinder("../companies.txt")

if len(sys.argv) > 1:
	content = sys.argv[1]

	(positive_score, negative_score) = get_opinion_score(op_finder, content)

	positive = colored("positive", "blue")
	negative = colored("negative", "red")

	print

	for (positive_word, data) in result[0].iteritems():
		print colored(positive_word, "blue"), data[0]

	for (negative_word, data) in result[1].iteritems():
		print colored(negative_word, "red"), data[0]

	result = cp_finder.feed(content)

	sorted_result = sorted([(company, data[0], data[1]) for (company, data) in result.iteritems()], key=sort_key, reverse=True)

	for (company, company_id, frequency) in sorted_result:
		print colored(company), company_id, frequency

	print "-" * 60

	print "%s score is %s." % (positive, colored(positive_score, "green"))
	print "%s score is %s." % (negative, colored(negative_score, "green"))
	print "so this is %s news of %s." % (positive if positive_score > negative_score else negative, colored(sorted_result[0][0]))

	print
else:
	conn = pymysql.connect(host="211.110.140.229", port=3306, user="soma", passwd="thak2013", database="stock", autocommit=True)
	cur = conn.cursor()

	cur.execute("select news_url, news_title, news_content from NEWS order by news_date desc limit 100")
	for (news_url, news_title, news_content) in cur:
		opinion_score = 0
		positive_words = []
		negative_words = []
		company_result = {}

		print "-" * 78

		print "news title: %s" % (colored(news_title))

		(positive_score, negative_score) = get_opinion_score(op_finder, news_title, 2, 2)

		result = op_finder.feed(news_title)

		print "positive score: %s" % (colored(positive_score, "blue")),
		for (positive_word, data) in result[0].iteritems():
			print "%s (%s times)" % (colored(positive_word, "blue"), colored(data[0], "green")),
		print

		print "negative_score: %s" % (colored(negative_score, "red")),
		for (negative_word, data) in result[1].iteritems():
			print "%s (%s times)" % (colored(negative_word, "red"), colored(data[0], "green")),
		print

		opinion_score += positive_score - negative_score

		(company, company_id, frequency) = get_most_frequent_company(cp_finder, news_title, 1)
		if company:
			company_result[company] = [company_id, frequency]

		if company:
			print "%s: %s[%d] (%s times)" % (colored("company", "pink"), colored(company), company_id, colored(frequency, "green"))
		else:
			print colored("no company found", "pink")

		print "news_content: %s" % (colored("..."))

		(positive_score, negative_score) = get_opinion_score(op_finder, news_content, 1, 1)

		result = op_finder.feed(news_content)

		print "positive score: %s" % (colored(positive_score, "blue")),
		for (positive_word, data) in result[0].iteritems():
			print "%s (%s times)" % (colored(positive_word, "blue"), colored(data[0], "green")),
		print

		print "negative_score: %s" % (colored(negative_score, "red")),
		for (negative_word, data) in result[1].iteritems():
			print "%s (%s times)" % (colored(negative_word, "red"), colored(data[0], "green")),
		print

		opinion_score += positive_score - negative_score

		(company, company_id, frequency) = get_most_frequent_company(cp_finder, news_content, 1)
		if company:
			if company in company_result:
				company_result[company][1] += frequency
			else:
				company_result[company] = [company_id, frequency]

		if company:
			print "%s: %s[%d] (%s times)" % (colored("company", "pink"), colored(company), company_id, colored(frequency, "green"))
		else:
			print colored("no company found", "pink")

		result = op_finder.feed(news_title + " " + news_content)

		for (positive_word, data) in result[0].iteritems():
			positive_words.append("%s : %d" % (positive_word, data[0]))

		for (negative_word, data) in result[1].iteritems():
			negative_words.append("%s : %d" % (negative_word, data[0]))

		sorted_result = sorted(company_result.iteritems(), key=lambda x: x[1][1], reverse=True)

		if (positive_words or negative_words) and sorted_result:
			db_query(conn, "insert into OPINION (opinion_company_id, opinion_value, news_url, opinion_positive, opinion_negative) values ('%d', '%d', '%s', '%s', '%s')" % (sorted_result[0][1][0], opinion_score, news_url, ", ".join(positive_words), ", ".join(negative_words)))