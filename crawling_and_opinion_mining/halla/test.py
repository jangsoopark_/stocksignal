# coding: utf-8

import re

def colored(content, color="white"):
	colors = {"gray": 90, "red": 91, "green": 92, "yellow": 93, "blue": 94, "pink": 95, "white": 97}

	if color in colors:
		return "\033[%sm%s\033[0m" % (colors[color], content)
	else:
		return content

def encode(string, encoding="utf-8"):
	if isinstance(string, str):
		return string
	elif isinstance(string, unicode):
		return string.encode(encoding)

	return string

def decode(string, encoding="utf-8"):
	if isinstance(string, str):
		return string.decode(encoding)
	elif isinstance(string, unicode):
		return string

	return string

def load_words_from_file(file_path, line_proc):
	words = []

	try:
		file = open(file_path, "rt")
	except IOError, e:
		print "cannot open file '%s'" % (file_path)

	for line in file:
		line = line.strip()
		if line and not line.startswith("#"):
			words.append(line_proc(line))

	file.close()

	return words

def find_word(content, word, flag=False):
	count = 0
	indexes = []

	SPECIAL_CHARACTERS = "!\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ "

	content = decode(content)
	word = decode(word.replace(" ", ""))

	content_length = len(content)
	word_length = len(word)

	content_index = 0
	word_index = 0

	match_start_index = 0

	while content_index < content_length:
		if content[content_index] == word[word_index]:
			if word_index == 0:
				match_start_index = content_index
			word_index += 1
		elif content[content_index] != " ":
			word_index = 0

		if word_index == word_length:
			if ((flag and match_start_index == 0 or content[match_start_index - 1] in SPECIAL_CHARACTERS) or
				(not flag)):
				count += 1
				indexes.append(match_start_index)
			word_index = 0
			
		content_index += 1

	return (count, indexes)

def split_by_sentence(content):
	result = []

	for sentence in re.findall(r"[\s\.]*(.+?\W\.)", content):
		result.append(sentence)

	return result

if __name__ == "__main__":
	LINE_SEPARATOR = colored("-" * 80, "green")
	# colored() testing
	print LINE_SEPARATOR
	print "# colored() testing..."

	print colored("default")
	print colored("gray", color="gray")
	print colored("red", color="red")
	print colored("green", color="green")
	print colored("yellow", color="yellow")
	print colored("blue", color="blue")
	print colored("pink", color="pink")
	print colored("white", color="white")

	# load_words_from_file() testing
	print LINE_SEPARATOR
	print "# load_words_from_file() testing..."

	print ">>> load_words_from_file('../test_words', lambda line: line[11:-1])"
	print load_words_from_file("../test_words", lambda line: line[11:-1])

	# find_word() testing
	print LINE_SEPARATOR
	print "# find_word() testing..."

	content1 = "주식회사 상보의 예상보다 높은 실적에 .."
	content2 = "삼성전자와 삼성 화재 간의 치열한 경쟁 .."
	company1 = "상보"
	company2 = "삼성 전자"
	company3 = "삼성화재"
	print ">>> find_word('%s', '%s')" % (content1, company1)
	print find_word(content1, company1, False)
	print ">>> find_word('%s', '%s')" % (content2, company2)
	print find_word(content2, company2, True)
	print ">>> find_word('%s', '%s')" % (content2, company3)
	print find_word(content2, company3, True)

	result = split_by_sentence("주가가 총 20.6% 올랐다. 이는 큰 변화이다. SBS 김할라 기자")
	for res in result:
		print res

	print LINE_SEPARATOR