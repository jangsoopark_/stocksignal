#-*-coding: utf-8-*-
# coding: utf-8
import re
import codecs
from bs4 import BeautifulSoup
import urllib2
import sys

sys.stdout = codecs.getwriter('utf-8')(sys.stdout)

def extractor(paged_url,fname):
	wf = codecs.open(fname, 'w', encoding='utf-8')
	paged_html = urllib2.urlopen(paged_url)
	paged_html_result = paged_html.read()
	soup = BeautifulSoup(paged_html_result,"lxml" ,from_encoding='utf-8')
	# [s.extract() for s in soup('script')] # script, 자바스크립트 등 제거
	# [s.extract() for s in soup('style')]	# CSS 등 style 코드 제거 
	#wf.write('%s' % soup.get_text(strip=True))
	
	for item in soup.find_all("a","bbs"):

		item_url = item['href']
		item_code = item_url.split('=')[1]
		item_name = item.get_text()

		print item_code+"|"+item_name
		wf.write(item_code+"|"+item_name+"\n")


if __name__=="__main__":

	url_kospi = "http://paxnet.moneta.co.kr/stock/searchStock/searchStock.jsp?section=0"
	url_kosdaq = "http://paxnet.moneta.co.kr/stock/searchStock/searchStock.jsp?section=1"
	extractor(url_kospi,"kospi")	
	# extractor(url_kosdaq,"kosdaq")
	






