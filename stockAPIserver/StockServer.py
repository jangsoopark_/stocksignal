# coding: utf-8

import socket
from StockAPI import StockAPI
import rsa
from Crypto.Cipher import AES
from Crypto import Random
import time


DEBUG = True
PORT = 4116


priv_key = rsa.key.PrivateKey(92025398516352833057483421158458980440871913197008455920483103521642106550992767915348696182601869258650607082222580330812659937930549384320678097717145680841077608291666361051972681816577132759564739651756921434893798432860408603418671962155309410319207569340012253716665444699734698772857826603833986134207, 65537, 61321714124962152256791712855502420538525067080070132574017999204932060289430476946323342707026660243748274438679559723925716345718678336858085232052816038308364555815275192549314110990034566429926735440934163980178476417909528112645561937627759867424974393684829879721080200646229882143382647719629772924761, 30967004747438698729304607228143036254147738762043539072586898225960025049144593868348730617231133230905976363240448462819671489299588771772639887493567435058852469, 2971724235743668884174210462490473047234937184523231351266683871116851682934948929310794939613200983707881808392832518013352615667844363562207203)
pub_key = rsa.key.PublicKey(92025398516352833057483421158458980440871913197008455920483103521642106550992767915348696182601869258650607082222580330812659937930549384320678097717145680841077608291666361051972681816577132759564739651756921434893798432860408603418671962155309410319207569340012253716665444699734698772857826603833986134207,65537)


current_random_key = ""

# def dongsam_encrypt(msg):
# 	global pub_key
# 	#padding
# 	block_size = 32
# 	padding = ' '
# 	forPadding = block_size-(len(msg)%block_size)
# 	msg += padding*forPadding

# 	random_key = Random.new().read(32)
# 	encrypted_key = rsa.encrypt(random_key, pub_key)
	
# 	cipher = AES.new(random_key)

# 	encrypt_msg = cipher.encrypt(msg)

# 	return_msg = encrypted_key+"|+|"+encrypt_msg
# 	return return_msg

def padding(msg):
	block_size = 32
	padding = ' '
	forPadding = block_size-(len(msg)%block_size)
	msg += padding*forPadding
	return msg

def dongsam_decrypt(msg):
	global priv_key
	global current_random_key

	msg_get = msg.split('|+|')
	encrypted_key = msg_get[0]
	encrypt_msg = msg_get[1]
	recovery_key = rsa.decrypt(encrypted_key,priv_key)
	current_random_key = recovery_key

	cipher = AES.new(recovery_key)
	decrypt_msg = cipher.decrypt(encrypt_msg).rstrip()
	return decrypt_msg

def dongsam_server_encrypt(msg):
	global current_random_key
	msg = padding(msg)
	cipher = AES.new(current_random_key)
	encrypt_msg = cipher.encrypt(msg).rstrip()
	return encrypt_msg


if __name__ == "__main__":
	stock = StockAPI(serverType=0,user="eastxhfl", password="qlqjs123", certpw="")

	server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_socket.bind(("", PORT))
	server_socket.listen(1)

	while True:
		conn, addr = server_socket.accept()
		if DEBUG:
			print "[!] Client Connected", addr

		while True:
			encryptFlag = 0
			data = conn.recv(1024)
			print data
			print len(str(data))
			print len(data)
			if not len(str(data)) == 6 :
				encryptFlag = 1

			if not data:
				if DEBUG:
					print "[!] Client Disconnected"
				break
			elif data == "close":
				if DEBUG:
					print "[!] Closing Server ..."
				conn.close()
				exit()
			
			if encryptFlag:	
				data = dongsam_decrypt(data)

			return_tuple = stock.getPrice(data)
			tryCount = 0
			while return_tuple:
				(hname, price, change) = return_tuple
				if not price or price == "" or price == None:
					print "-------time limit--------",tryCount
					if tryCount > 2:
						hname = "0"
						price = "0"
						change = "0"
						print "set 0"
						break
					time.sleep(1)
					return_tuple = stock.getPrice(data)
					tryCount += 1
					continue
				else:
					break

			if encryptFlag:
				conn.send(dongsam_server_encrypt(price))
			else:
				conn.send(price)

		conn.close()
