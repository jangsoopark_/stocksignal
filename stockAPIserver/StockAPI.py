# coding: utf-8

import pythoncom
import win32com.client

DEBUG = True

def debug_print(handler_name, *args):
	print "[!] %s" % (handler_name)

	for arg in args:
		print "\t",

		for v in arg:
			if isinstance(v, unicode):
				print v.encode("euc-kr"),
			else:
				print v,
				
		print

class XASessionEvents:
	def OnLogin(self, *args):
		if DEBUG:
			debug_print("XASessionEvents.OnLogin", args)
		self.flag = False

	def OnLogout(self, *args):
		if DEBUG:
			debug_print("XASessionEvents.OnLogout", args)
		self.flag = False

	def OnDisconnect(self, *args):
		if DEBUG:
			debug_print("XASessionEvents.OnDisconnect", args)
		self.flag = False

class XAQueryEvents:
	def OnReceiveData(self, *args):
		if DEBUG:
			debug_print("XAQueryEvents.OnReceiveData", args)
		self.session.flag = False

	def OnReceiveMessage(self, *args):
		if DEBUG:
			debug_print("XAQueryEvents.OnReceiveMessage", args)
		self.session.flag = False

class StockAPI:
	def __init__(self, serverType, user, password, certpw):
		self.XASession = win32com.client.DispatchWithEvents("XA_Session.XASession", XASessionEvents)
		self.XAQuery = win32com.client.DispatchWithEvents("XA_DataSet.XAQuery", XAQueryEvents)

		self.XAQuery.session = self.XASession

		
		if serverType:
			self.XASeesion.ConnectServer("hts.etrade.co.kr",20001)
		else:
			self.XASession.ConnectServer("demo.etrade.co.kr", 20001)

		if DEBUG:
			print "[*] Logging In ..."

		self.XASession.Login(user, password, certpw, 0, 1)
		self.XASession.flag = True
		while self.XASession.flag:
			pythoncom.PumpWaitingMessages()

		if DEBUG:
			print "[*] Login Success"

	def trading(self, IsuNo, OrdQty, OrdPrc, BnsTpCode):
		self.XAQuery.LoadFromResFile("C:\\ETRADE\\xingAPI\\Res\\CSPAT00600.res")
		OrdprcPtnCode = 03
		self.XAQuery.SetFieldData("CSPAT00600InBlock1", "AcntNo", 0, "55956003402")
		self.XAQuery.SetFieldData("CSPAT00600InBlock1", "InptPwd", 0, "0000")
		self.XAQuery.SetFieldData("CSPAT00600InBlock1", "IsuNo", 0, "A"+IsuNo)
		self.XAQuery.SetFieldData("CSPAT00600InBlock1", "OrdQty", 0, OrdQty)
		self.XAQuery.SetFieldData("CSPAT00600InBlock1", "OrdPrc", 0, OrdPrc)
		self.XAQuery.SetFieldData("CSPAT00600InBlock1", "BnsTpCode", 0, BnsTpCode)
		self.XAQuery.SetFieldData("CSPAT00600InBlock1", "OrdprcPtnCode", 0, OrdprcPtnCode)
		self.XAQuery.SetFieldData("CSPAT00600InBlock1", "LoanDt", 0, " ")
		self.XAQuery.SetFieldData("CSPAT00600InBlock1", "OrdCndiTpCode", 0, 0)

		if DEBUG:
			print "[*] Requesting ..."

		self.XAQuery.Request(False)
		self.XAQuery.session.flag = True
		while self.XAQuery.session.flag:
			pythoncom.PumpWaitingMessages()

		if DEBUG:
			print "[*] Request Success"

		RecCnt = self.XAQuery.GetFieldData("CSPAT00600OutBlock1", "RecCnt", 0)
		AcntNo = self.XAQuery.GetFieldData("CSPAT00600OutBlock1", "AcntNo", 0)
		OrdPrc_R = self.XAQuery.GetFieldData("CSPAT00600OutBlock1", "OrdPrc", 0)

		if DEBUG:
			print "[*] RecCnt: %s, AcntNo: %s, OrdPrc_R: %s" % (RecCnt, AcntNo, OrdPrc_R)

		return (RecCnt, AcntNo, OrdPrc_R)

	def getPrice(self, shcode):
		self.XAQuery.LoadFromResFile("C:\\ETRADE\\xingAPI\\Res\\t1102.res")
		self.XAQuery.SetFieldData("t1102InBlock", "shcode", 0, shcode)

		if DEBUG:
			print "[*] Requesting ..."

		self.XAQuery.Request(False)
		self.XAQuery.session.flag = True
		while self.XAQuery.session.flag:
			pythoncom.PumpWaitingMessages()

		if DEBUG:
			print "[*] Request Success"

		hname = self.XAQuery.GetFieldData("t1102OutBlock", "hname", 0)
		price = self.XAQuery.GetFieldData("t1102OutBlock", "price", 0)
		change = self.XAQuery.GetFieldData("t1102OutBlock", "change", 0)
		
		if DEBUG:
			print "[*] Name: %s, Price: %s, Change: %s" % (hname, price, change)

		return (hname, price, change)

	def getPrice_all(self, shcode):
		self.XAQuery.LoadFromResFile("C:\\ETRADE\\xingAPI\\Res\\t1102.res")
		self.XAQuery.SetFieldData("t1102InBlock", "shcode", 0, shcode)

		if DEBUG:
			print "[*] Requesting ..."

		self.XAQuery.Request(False)
		self.XAQuery.session.flag = True
		while self.XAQuery.session.flag:
			pythoncom.PumpWaitingMessages()

		if DEBUG:
			print "[*] Request Success"

		return_dict = {}

		return_dict["hname"] = self.XAQuery.GetFieldData("t1102OutBlock", "hname", 0)
		return_dict["price"] = self.XAQuery.GetFieldData("t1102OutBlock", "price", 0)
		return_dict["sign"] = self.XAQuery.GetFieldData("t1102OutBlock", "sign", 0)
		return_dict["change"] = self.XAQuery.GetFieldData("t1102OutBlock", "change", 0)
		return_dict["diff"] = self.XAQuery.GetFieldData("t1102OutBlock", "diff", 0)
		return_dict["recprice"] = self.XAQuery.GetFieldData("t1102OutBlock", "recprice", 0)
		return_dict["avg"] = self.XAQuery.GetFieldData("t1102OutBlock", "avg", 0)
		return_dict["uplmtprice"] = self.XAQuery.GetFieldData("t1102OutBlock", "uplmtprice", 0)
		return_dict["dnlmtprice"] = self.XAQuery.GetFieldData("t1102OutBlock", "dnlmtprice", 0)
		return_dict["jnilvolume"] = self.XAQuery.GetFieldData("t1102OutBlock", "jnilvolume", 0)
		return_dict["volumediff"] = self.XAQuery.GetFieldData("t1102OutBlock", "volumediff", 0)
		return_dict["open_S"] = self.XAQuery.GetFieldData("t1102OutBlock", "open", 0)
		return_dict["opentime"] = self.XAQuery.GetFieldData("t1102OutBlock", "opentime", 0)
		return_dict["high"] = self.XAQuery.GetFieldData("t1102OutBlock", "high", 0)
		return_dict["hightime"] = self.XAQuery.GetFieldData("t1102OutBlock", "hightime", 0)
		return_dict["low"] = self.XAQuery.GetFieldData("t1102OutBlock", "low", 0)
		return_dict["lowtime"] = self.XAQuery.GetFieldData("t1102OutBlock", "lowtime", 0)
		return_dict["high52w"] = self.XAQuery.GetFieldData("t1102OutBlock", "high52w", 0)
		return_dict["high52wdate"] = self.XAQuery.GetFieldData("t1102OutBlock", "high52wdate", 0)
		return_dict["low52w"] = self.XAQuery.GetFieldData("t1102OutBlock", "low52w", 0)
		return_dict["low52wdate"] = self.XAQuery.GetFieldData("t1102OutBlock", "low52wdate", 0)
		return_dict["exhratio"] = self.XAQuery.GetFieldData("t1102OutBlock", "exhratio", 0)
		return_dict["per"] = self.XAQuery.GetFieldData("t1102OutBlock", "per", 0)
		return_dict["pbrx"] = self.XAQuery.GetFieldData("t1102OutBlock", "pbrx", 0)
		return_dict["listing"] = self.XAQuery.GetFieldData("t1102OutBlock", "listing", 0)
		return_dict["jkrate"] = self.XAQuery.GetFieldData("t1102OutBlock", "jkrate", 0)
		return_dict["memedan"] = self.XAQuery.GetFieldData("t1102OutBlock", "memedan", 0)
		return_dict["offernocd1"] = self.XAQuery.GetFieldData("t1102OutBlock", "offernocd1", 0)
		return_dict["bidnocd1"] = self.XAQuery.GetFieldData("t1102OutBlock", "bidnocd1", 0)
		return_dict["offerno1"] = self.XAQuery.GetFieldData("t1102OutBlock", "offerno1", 0)
		return_dict["bidno1"] = self.XAQuery.GetFieldData("t1102OutBlock", "bidno1", 0)
		return_dict["dvol1"] = self.XAQuery.GetFieldData("t1102OutBlock", "dvol1", 0)
		return_dict["svol1"] = self.XAQuery.GetFieldData("t1102OutBlock", "svol1", 0)
		return_dict["dcha1"] = self.XAQuery.GetFieldData("t1102OutBlock", "dcha1", 0)
		return_dict["scha1"] = self.XAQuery.GetFieldData("t1102OutBlock", "scha1", 0)
		return_dict["ddiff1"] = self.XAQuery.GetFieldData("t1102OutBlock", "ddiff1", 0)
		return_dict["sdiff1"] = self.XAQuery.GetFieldData("t1102OutBlock", "sdiff1", 0)
		return_dict["fwdvl"] = self.XAQuery.GetFieldData("t1102OutBlock", "fwdvl", 0)
		return_dict["ftradmdcha"] = self.XAQuery.GetFieldData("t1102OutBlock", "ftradmdcha", 0)
		return_dict["ftradmddiff"] = self.XAQuery.GetFieldData("t1102OutBlock", "ftradmddiff", 0)
		return_dict["fwsvl"] = self.XAQuery.GetFieldData("t1102OutBlock", "fwsvl", 0)
		return_dict["ftradmscha"] = self.XAQuery.GetFieldData("t1102OutBlock", "ftradmscha", 0)
		return_dict["ftradmsdiff"] = self.XAQuery.GetFieldData("t1102OutBlock", "ftradmsdiff", 0)
		return_dict["vol"] = self.XAQuery.GetFieldData("t1102OutBlock", "vol", 0)
		return_dict["shcode"] = self.XAQuery.GetFieldData("t1102OutBlock", "shcode", 0)
		return_dict["value"] = self.XAQuery.GetFieldData("t1102OutBlock", "value", 0)
		return_dict["jvolume"] = self.XAQuery.GetFieldData("t1102OutBlock", "jvolume", 0)
		return_dict["highyear"] = self.XAQuery.GetFieldData("t1102OutBlock", "highyear", 0)
		return_dict["highyeardate"] = self.XAQuery.GetFieldData("t1102OutBlock", "highyeardate", 0)
		return_dict["lowyear"] = self.XAQuery.GetFieldData("t1102OutBlock", "lowyear", 0)
		return_dict["lowyeardate"] = self.XAQuery.GetFieldData("t1102OutBlock", "lowyeardate", 0)
		return_dict["target"] = self.XAQuery.GetFieldData("t1102OutBlock", "target", 0)
		return_dict["capital"] = self.XAQuery.GetFieldData("t1102OutBlock", "capital", 0)
		return_dict["abscnt"] = self.XAQuery.GetFieldData("t1102OutBlock", "abscnt", 0)
		return_dict["parprice"] = self.XAQuery.GetFieldData("t1102OutBlock", "parprice", 0)
		return_dict["gsmm"] = self.XAQuery.GetFieldData("t1102OutBlock", "gsmm", 0)
		return_dict["subprice"] = self.XAQuery.GetFieldData("t1102OutBlock", "subprice", 0)
		return_dict["listdate"] = self.XAQuery.GetFieldData("t1102OutBlock", "listdate", 0)
		return_dict["name"] = self.XAQuery.GetFieldData("t1102OutBlock", "name", 0)
		return_dict["bfsales"] = self.XAQuery.GetFieldData("t1102OutBlock", "bfsales", 0)
		return_dict["bfoperatingincome"] = self.XAQuery.GetFieldData("t1102OutBlock", "bfoperatingincome", 0)
		return_dict["bfordinaryincome"] = self.XAQuery.GetFieldData("t1102OutBlock", "bfordinaryincome", 0)
		return_dict["bfnetincome"] = self.XAQuery.GetFieldData("t1102OutBlock", "bfnetincome", 0)
		return_dict["bfeps"] = self.XAQuery.GetFieldData("t1102OutBlock", "bfeps", 0)
		return_dict["name2"] = self.XAQuery.GetFieldData("t1102OutBlock", "name2", 0)
		return_dict["bfsales2"] = self.XAQuery.GetFieldData("t1102OutBlock", "bfsales2", 0)
		return_dict["bfoperatingincome2"] = self.XAQuery.GetFieldData("t1102OutBlock", "bfoperatingincome2", 0)
		return_dict["bfordinaryincome2"] = self.XAQuery.GetFieldData("t1102OutBlock", "bfordinaryincome2", 0)
		return_dict["bfnetincome2"] = self.XAQuery.GetFieldData("t1102OutBlock", "bfnetincome2", 0)
		return_dict["bfeps2"] = self.XAQuery.GetFieldData("t1102OutBlock", "bfeps2", 0)
		return_dict["salert"] = self.XAQuery.GetFieldData("t1102OutBlock", "salert", 0)
		return_dict["opert"] = self.XAQuery.GetFieldData("t1102OutBlock", "opert", 0)
		return_dict["ordrt"] = self.XAQuery.GetFieldData("t1102OutBlock", "ordrt", 0)
		return_dict["netrt"] = self.XAQuery.GetFieldData("t1102OutBlock", "netrt", 0)
		return_dict["epsrt"] = self.XAQuery.GetFieldData("t1102OutBlock", "epsrt", 0)
		return_dict["info1"] = self.XAQuery.GetFieldData("t1102OutBlock", "info1", 0)
		return_dict["info2"] = self.XAQuery.GetFieldData("t1102OutBlock", "info2", 0)
		return_dict["info3"] = self.XAQuery.GetFieldData("t1102OutBlock", "info3", 0)
		return_dict["info4"] = self.XAQuery.GetFieldData("t1102OutBlock", "info4", 0)
		return_dict["janginfo"] = self.XAQuery.GetFieldData("t1102OutBlock", "janginfo", 0)
		return_dict["t_per"] = self.XAQuery.GetFieldData("t1102OutBlock", "t_per", 0)

		# if DEBUG:
		# 	print "[*] Name: %s, Price: %s, Change: %s" % (hname, price, change)

		return return_dict

	def getPastPrice(self, shcode):
		self.XAQuery.LoadFromResFile("C:\\ETRADE\\xingAPI\\Res\\t1302.res")
		self.XAQuery.SetFieldData("t1302InBlock", "shcode", 0, shcode)
		self.XAQuery.SetFieldData("t1302InBlock", "gubun", 0, 0) # for every 30 second
		self.XAQuery.SetFieldData("t1302InBlock", "time", 0, 0)
		self.XAQuery.SetFieldData("t1302InBlock", "cnt", 0, 0)

		if DEBUG:
			print "[*] Requesting ..."

		self.XAQuery.Request(False)
		self.XAQuery.session.flag = True
		while self.XAQuery.session.flag:
			pythoncom.PumpWaitingMessages()

		if DEBUG:
			print "[*] Request Success"

		chetime = self.XAQuery.GetFieldData("t1302OutBlock1", "chetime", 0)
		close = self.XAQuery.GetFieldData("t1302OutBlock1", "close", 0)
		sign = self.XAQuery.GetFieldData("t1302OutBlock1", "sign", 0)
		change = self.XAQuery.GetFieldData("t1302OutBlock1", "change", 0)
		diff = self.XAQuery.GetFieldData("t1302OutBlock1", "diff", 0)
		chdegree = self.XAQuery.GetFieldData("t1302OutBlock1", "chdegree", 0)
		mdvolume = self.XAQuery.GetFieldData("t1302OutBlock1", "mdvolume", 0)
		msvolume = self.XAQuery.GetFieldData("t1302OutBlock1", "msvolume", 0)
		revolume = self.XAQuery.GetFieldData("t1302OutBlock1", "revolume", 0)
		mdchecnt = self.XAQuery.GetFieldData("t1302OutBlock1", "mdchecnt", 0)
		mschecnt = self.XAQuery.GetFieldData("t1302OutBlock1", "mschecnt", 0)
		rechecnt = self.XAQuery.GetFieldData("t1302OutBlock1", "rechecnt", 0)
		volume = self.XAQuery.GetFieldData("t1302OutBlock1", "volume", 0)
		open_price = self.XAQuery.GetFieldData("t1302OutBlock1", "open", 0)
		high = self.XAQuery.GetFieldData("t1302OutBlock1", "high", 0)
		low = self.XAQuery.GetFieldData("t1302OutBlock1", "low", 0)
		cvolume = self.XAQuery.GetFieldData("t1302OutBlock1", "cvolume", 0)
		mdchecnttm = self.XAQuery.GetFieldData("t1302OutBlock1", "mdchecnttm", 0)
		mschecnttm = self.XAQuery.GetFieldData("t1302OutBlock1", "mschecnttm", 0)
		totofferrem = self.XAQuery.GetFieldData("t1302OutBlock1", "totofferrem", 0)
		totbidrem = self.XAQuery.GetFieldData("t1302OutBlock1", "totbidrem", 0)
		mdvolumetm = self.XAQuery.GetFieldData("t1302OutBlock1", "mdvolumetm", 0)
		msvolumetm = self.XAQuery.GetFieldData("t1302OutBlock1", "msvolumetm", 0)

		if DEBUG:
			print "[*] chetime: %s, open_price: %s, diff: %s" % (chetime, open_price, diff)

		return (chetime, open_price, diff)

if __name__ == "__main__":
	stock = StockAPI(serverType=0, user="eastxhfl", password="qlqjs123", certpw="") # serverType == 1 -> real stock server,   serverType == 0 -> demo, copy server

	stock.trading(IsuNo = "005930", OrdQty = "1", OrdPrc = "1460000", BnsTpCode = "1" )
	(hname, price, change) = stock.getPrice(shcode="000590")
	print hname, price, change
	stock.getPastPrice("005930")
