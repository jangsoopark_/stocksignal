#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <time.h>
#include <sys/fcntl.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <net/if.h>
#include <errno.h>
#include "FutOrd.h"
#include "aslib.h"

#define MAX_BUFF_LEN   4096

#define	SERVIP	"61.106.5.53" /* check */
#define CONFIG_PATH "/htsteam/home/example/customer/outer_etk_simul"

AS_HANDLE   AH;

typedef struct _ORD
{
	char	ordno[10];
	char	qty[16];
}ORD;

int SendPacket(char *ordpacket)
{
	char sbuff[4096+1], rbuff[4096+1], tmp[48];
    CCEAT00300InBlock1 InBlock1;
    CCEAT00300OutBlock1 OutBlock1;
    CCEAT00300OutBlock2 *OutBlock2;
	SB_NHAP	sbhd;
	ORD		*ord;
	int		rc, rlen, slen, roklen;
    char      encode[1024], decode[4096];

	ord = (ORD *)ordpacket;

    /* =============================================== */
    /* 고객 환경 Load 및 로그인 - plase, check it out  */
    /* =============================================== */
	/*  rc = -3 : 공인인증실패                         */
	/*  rc = -2 : 로그인실패                           */
	/*  rc = -1 : 서버접속실패                         */
	/*  rc =  0 : TR전송실패                           */
	/*  rc =  1 : 성공                                 */
    rc = API_LOGIN_SIMUL(AH,  CONFIG_PATH, "LOGIN.cfg");
	if(rc <= 0)
	{
		printf("rc:%d [Error return]\n", rc);
		exit(0);
	}

    memset((char *)&sbhd, 0x20, sizeof(sbhd));
    memset((char *)&InBlock1, 0x20, sizeof(InBlock1));
    memset(sbuff, 0x00, sizeof(sbuff));
    memset(rbuff, 0x00, sizeof(rbuff));

    /* =============================================== */
    /* 헤더 고객 입력영역 please check it out          */
    /* =============================================== */
	/* User Define - check it out */
    memcpy(sbhd.UserID, "etk2013 ", sizeof(sbhd.UserID));  /* ID check */
    memcpy(sbhd.IPAddr, "192168022160    ", sizeof(sbhd.IPAddr));  /* 계좌check*/
    memcpy(sbhd.BranchNo, "004", sizeof(sbhd.BranchNo)); /* 관리점 check */
    /* =============================================== */

    memcpy(sbhd.MediaETK, "s ", sizeof(sbhd.MediaETK));
    memcpy(sbhd.ServiceCode, "CCEAT00300", sizeof(sbhd.ServiceCode));
	sbhd.FuncCode[0] = 'C';
	sbhd.Cont[0] = 'N';
	sbhd.Lang[0] = 'K';
	sbhd.Loan[0] = '0';
    memcpy(sbhd.MediaComm, "41", sizeof(sbhd.MediaComm));
	sbhd.BussGb[0] = '0';

    /* =============================================== */
    /* 헤더 고객 입력영역 please check it out          */
    /* =============================================== */
    memcpy(InBlock1.AcntNo, "00451707274         ", sizeof(InBlock1.AcntNo));/* 계좌check*/
    memcpy(InBlock1.Pwd, "0001    ", sizeof(InBlock1.Pwd)); /* 비밀번호 check */
    memcpy(InBlock1.FnoIsuNo, "101H9000    ", sizeof(InBlock1.FnoIsuNo)); /* 종목check */
    /* =============================================== */

    memcpy(InBlock1.RecCnt, "00001", sizeof(InBlock1.RecCnt));
    memcpy(InBlock1.OrdMktCode, "40", sizeof(InBlock1.OrdMktCode));
    memcpy(InBlock1.FnoOrdPtnCode, "00", sizeof(InBlock1.FnoOrdPtnCode));
    memcpy(InBlock1.CommdaCode, "41", sizeof(InBlock1.CommdaCode)); 
    memcpy(InBlock1.DscusBnsCmpltTime, "         ", sizeof(InBlock1.DscusBnsCmpltTime)); 
    memcpy(InBlock1.GrpId, "                    ", sizeof(InBlock1.GrpId)); 
    memcpy(InBlock1.OrdSeqno, "0000000000", sizeof(InBlock1.OrdSeqno)); 
    memcpy(InBlock1.PtflNo, "0000000000", sizeof(InBlock1.PtflNo)); 
    memcpy(InBlock1.BskNo, "0000000000", sizeof(InBlock1.BskNo)); 
    memcpy(InBlock1.TrchNo, "0000000000", sizeof(InBlock1.TrchNo)); 
    memcpy(InBlock1.ItemNo, "0000000000000000", sizeof(InBlock1.ItemNo)); 
    memcpy(InBlock1.MgempNo, "         ", sizeof(InBlock1.MgempNo)); 
    memcpy(InBlock1.FundId, "0           ", sizeof(InBlock1.FundId)); 
    memcpy(InBlock1.FundOrdNo, "0000000000", sizeof(InBlock1.FundOrdNo)); 
    memcpy(InBlock1.FundOrgOrdNo, "0000000000", sizeof(InBlock1.FundOrgOrdNo)); 

    memcpy(InBlock1.OrgOrdNo, ord->ordno, sizeof(InBlock1.OrgOrdNo)); 
    memcpy(InBlock1.CancQty, ord->qty, sizeof(InBlock1.CancQty));

	memcpy(sbuff, &sbhd, sizeof(sbhd));
	memcpy(sbuff+sizeof(sbhd), &InBlock1, sizeof(InBlock1));

	/* 암호화 */
    slen = AsEncode(sbuff, sizeof(SB_NHAP)+sizeof(InBlock1), encode);

	rc = AsCallTran(AH, "a7810", encode, slen, rbuff, 5);
	/* 복호화 */
    rlen = AsDecode(rbuff,decode,rc);

    printf("[%d][%s]\n", strlen(decode), decode);

    OutBlock2 = (CCEAT00300OutBlock2 *)&decode[sizeof(CCEAT00300OutBlock1)];

    printf("주문접수번호[%.*s]\n", sizeof(OutBlock2->OrdNo), OutBlock2->OrdNo);
    printf("[%.5s] %s\n", AH->m_MsgCode,  AH->m_MsgStr);

    AsTerminate(AH);

	return 1;
}

int main(int argc, char *argv[])
{
	int    sfd, sz;
	int    len;
	long   lordno;
	long   lqty;
	char buff[128], temp[40];

	if(argc != 3)
	{
		printf("Usage : a1303 원주문번호 수량\n");
        printf("    샘플소스내의 check 항목을 확인하세요.\n");
		return 0;
	}

    AH = AsInitial(0, NULL);
    if (AH == NULL)
    {
        printf("AS_HANDLE initial Error\n");
        exit(0);
    }

	lordno	= atol(argv[1]);
	lqty  	= atol(argv[2]);

	sprintf(buff, "%010d%016d", lordno, lqty);
	/*
	printf("Send:[%s]\n", buff);
	*/

	SendPacket(buff);

	return 0;
}
