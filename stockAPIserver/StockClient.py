import socket
import rsa
from Crypto.Cipher import AES
from Crypto import Random

pub_key = rsa.key.PublicKey(92025398516352833057483421158458980440871913197008455920483103521642106550992767915348696182601869258650607082222580330812659937930549384320678097717145680841077608291666361051972681816577132759564739651756921434893798432860408603418671962155309410319207569340012253716665444699734698772857826603833986134207,65537)

current_random_key = ""
semaphore = 0

def padding(msg):
	block_size = 32
	padding = ' '
	forPadding = block_size-(len(msg)%block_size)
	msg += padding*forPadding
	return msg

def dongsam_encrypt(msg):
	global pub_key
	global current_random_key
	global semaphore
	#padding
	msg = padding(msg)

	if semaphore==0:
		random_key = Random.new().read(32)
		current_random_key = random_key
	else:
		random_key = current_random_key
	semaphore+=1
	encrypted_key = rsa.encrypt(random_key, pub_key)
	
	cipher = AES.new(random_key)

	encrypt_msg = cipher.encrypt(msg)

	return_msg = encrypted_key+"|+|"+encrypt_msg
	return return_msg

def dongsam_client_decrypt(msg):
	global current_random_key
	global semaphore
	cipher = AES.new(current_random_key)
	semaphore -= 1
	decrypt_msg = cipher.decrypt(msg).rstrip()
	return decrypt_msg

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(("1.234.36.27", 4116))

while True:
	data = raw_input("etrade> ")

	if data == "exit":
		break
	elif data == "close":
		client_socket.send("close")
		break

	client_socket.send(dongsam_encrypt(data))
	
	price = dongsam_client_decrypt(client_socket.recv(1024))
	print price

client_socket.close()
