# coding: utf-8

import pythoncom
import win32com.client

DEBUG = True

def debug_print(handler_name, *args):
	print "[!] %s" % (handler_name)

	for arg in args:
		print "\t",

		for v in arg:
			if isinstance(v, unicode):
				print v.encode("euc-kr"),
			else:
				print v,
				
		print

class XASessionEvents:
	def OnLogin(self, *args):
		if DEBUG:
			debug_print("XASessionEvents.OnLogin", args)
		self.flag = False

	def OnLogout(self, *args):
		if DEBUG:
			debug_print("XASessionEvents.OnLogout", args)
		self.flag = False

	def OnDisconnect(self, *args):
		if DEBUG:
			debug_print("XASessionEvents.OnDisconnect", args)
		self.flag = False

class XAQueryEvents:
	def OnReceiveData(self, *args):
		if DEBUG:
			debug_print("XAQueryEvents.OnReceiveData", args)
		self.session.flag = False

	def OnReceiveMessage(self, *args):
		if DEBUG:
			debug_print("XAQueryEvents.OnReceiveMessage", args)
		self.session.flag = False

class StockAPI:
	def __init__(self, serverType, user, password, certpw):
		self.XASession = win32com.client.DispatchWithEvents("XA_Session.XASession", XASessionEvents)
		self.XAQuery = win32com.client.DispatchWithEvents("XA_DataSet.XAQuery", XAQueryEvents)

		self.XAQuery.session = self.XASession

		self.XAQuery.LoadFromResFile("C:\\ETRADE\\xingAPI\\Res\\t1102.res")
		if serverType:
			self.XASeesion.ConnectServer("hts.etrade.co.kr",20001)
		else:
			self.XASession.ConnectServer("demo.etrade.co.kr", 20001)

		if DEBUG:
			print "[*] Logging In ..."

		self.XASession.Login(user, password, certpw, 0, 1)
		self.XASession.flag = True
		while self.XASession.flag:
			pythoncom.PumpWaitingMessages()

		if DEBUG:
			print "[*] Login Success"

	def getPrice(self, shcode):
		self.XAQuery.SetFieldData("t1102InBlock", "shcode", 0, shcode)

		if DEBUG:
			print "[*] Requesting ..."

		self.XAQuery.Request(False)
		self.XAQuery.session.flag = True
		while self.XAQuery.session.flag:
			pythoncom.PumpWaitingMessages()

		if DEBUG:
			print "[*] Request Success"

		hname = self.XAQuery.GetFieldData("t1102OutBlock", "hname", 0)
		price = self.XAQuery.GetFieldData("t1102OutBlock", "price", 0)
		change = self.XAQuery.GetFieldData("t1102OutBlock", "change", 0)

		if DEBUG:
			print "[*] Name: %s, Price: %s, Change: %s" % (hname, price, change)

		return (hname, price, change)

if __name__ == "__main__":
	stock = StockAPI(serverType=0, user="eastxhfl", password="qlqjs123", certpw="") # serverType == 1 -> real stock server,   serverType == 0 -> demo, copy server

	(hname, price, change) = stock.getPrice(shcode="000590")
	print hname, price, change
