# coding: utf-8

import re

SPECIAL_CHARACTERS = "!\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ "

def encode_text(text, encoding="utf-8"):
	if isinstance(text, unicode):
		return text.encode(encoding)
	else:
		return text

def decode_text(text, encoding="utf-8"):
	if isinstance(text, str):
		return text.decode(encoding)
	else:
		return text

def find_word(content, word, flag=False):
	count = 0
	indexes = []
	content = decode_text(content)
	word = decode_text(word.replace(" ", ""))
	content_length = len(content)
	word_length = len(word)
	content_index = 0
	word_index = 0
	match_start_index = 0

	while content_index < content_length:
		if content[content_index] == word[word_index]:
			if word_index == 0:
				match_start_index = content_index
			word_index += 1
		elif content[content_index] != " ":
			word_index = 0
		if word_index == word_length:
			if (not flag or
				flag and match_start_index == 0 or
				content[match_start_index - 1] in SPECIAL_CHARACTERS):
				count += 1
				indexes.append(match_start_index)
			word_index = 0
		content_index += 1

	return (count, indexes)

def split_sentence(content):
	result = []

	for sentence in re.findall(r"[\s\.]*(.+?\W\.)", content):
		result.append(sentence)

	return result

if __name__ == "__main__":
	print type(encode_text("문자열"))
	print type(encode_text(u"유니코드 문자열"))

	print type(decode_text("문자열"))
	print type(decode_text(u"유니코드 문자열"))

	content = "-삼성 전자에서 기존의 예상보다 높은 실적을 기록하며,"\
			"삼성전자가 주식회사 상보를 앞지를 것으로 큰기대를 받고 있습니다."
	word1 = "삼성전자"
	word2 = "상보"
	word3 = "기대"

	print find_word(content, word1, flag=True)
	print find_word(content, word2, flag=True)
	print find_word(content, word3, flag=False)

	content = "[아시아경제 김지은 기자]우리나라의 미국 국채 보유액이 사상 최대치를 기록해 미국 국채 총액의 0.98\%를 차지했다.18일(현지시간) 미국 재무부가 발표한 국채 보고서에 따르면 지난 9월 우리나라가 보유한 미국 국채는 556억달러로 사상 최대치에 도달했다. 한국의 미국 국채 보유액은 지난해 2월 508억달러까지 상승하다 같은 해 3월 427억달러로 급감한 뒤 지난해 9월에는 421억달러까지 감소했다. 그러나 올해 2월 다시 500억달러를 넘어선 후 8개월 연속 500억달러 이상을 유지해왔다.우리나라의 미국 국채 보유 세계 순위도 8월 22위에서 9월에는 프랑스, 터키를 제치고 20위에 올랐다. 중국이 1조 2938억달러로 1위, 일본이 1조 1781억달러로 2위를 차지했으며, 두 국가가 가진 미국 국채는 전체의 43.7\%에 달했다.한편 지난 9월 말 외국인이 보유한 미국 국채는 총 5조 6529억달러로 전달보다 571억달러(1\%) 증가했다.전문가들은 연방 정부 폐쇄 사태와 국가 디폴트(채무불이행) 위기에도 불구하고 외국 투자자들이 여전히 미국 국채를 안정적 투자 수단으로 여겨 외국인 보유가 꾸준히 증가하고 있는 것으로 분석했다. 김지은 기자 muse86i@asiae.co.kr"

	for sentence in split_sentence(content):
		print sentence