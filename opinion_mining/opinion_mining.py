# coding: utf-8

import os
from core import *

def opinion_words_line_proc(line):
	line = line.strip()
	if line and not line.startswith("#"):
		return line

def companies_line_proc(line):
	line = line.strip()
	if line and not line.startswith("#"):
		index = line.find("|")
		if index >= 0:
			return (line[:index], line[index + 1:])

_DEBUG_ = True

if __name__ == "__main__":
	cwd = os.path.dirname(os.path.abspath(__file__))

	positive_words = load_words_from_file(cwd + "/data/positive_words.txt", opinion_words_line_proc)
	negative_words = load_words_from_file(cwd + "/data/negative_words.txt", opinion_words_line_proc)
	companies = load_words_from_file(cwd + "/data/companies.txt", companies_line_proc)
	
	do(positive_words, negative_words, companies)