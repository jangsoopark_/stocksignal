# coding: utf-8

from utils import *
from strutils import *
from datetime import datetime
import pymysql

_DEBUG_ = True

def load_words_from_file(file_path, line_proc):
	words = []

	try:
		file = open(file_path, "rt")
	except IOError, e:
		print colored_text("cannot open file '%s'" % (file_path), "red")

		return words

	for line in file:
		result = line_proc(line)
		if result:
			words.append(result)

	file.close()

	return words

def find_opinion_words(content, opinion_words):
	result = []

	for opinion_word in opinion_words:
		count, indexes = find_word(content, opinion_word, flag=False)
		if count > 0:
			result.append((opinion_word, count))

	return result

def find_companies(content, companies):
	result = []

	for (company_id, company) in companies:
		count, indexes = find_word(content, company, flag=True)
		if count > 0:
			result.append((int(company_id), company, count))

	return result

def get_opinion_data(content, opinion_words, multiple=1):
	opinion_score = 0
	opinion_data = []

	result = find_opinion_words(content, opinion_words)

	for (opinion_word, count) in result:
		opinion_score += count
		opinion_data.append("%s : %d" % (opinion_word, count))

	return (opinion_score * multiple, opinion_data)

def get_most_frequent_company_data(content, companies):
	result = sorted(find_companies(content, companies), key=lambda x: x[2], reverse=True)

	if result:
		return result[0]
	else:
		return (None, None, None)

def check_already_exists(conn, news_url):
	cur = conn.cursor()
	cur.execute("select count(*) from OPINION where news_url = '%s'" % (news_url))
	result = cur.fetchone()[0]
	cur.close()

	return result > 0

def insert_to_db(conn, opinion_company_id, opinion_value, news_url, opinion_positive, opinion_negative, sentence):
	query = ("insert into OPINION "
		"(opinion_company_id, opinion_value, opinion_date, news_url, opinion_positive, opinion_negative, sentence) "
		"values ('%d', '%d', '%s', '%s', '%s', '%s', '%s')" %
		(opinion_company_id, opinion_value, datetime.now(), news_url, ", ".join(opinion_positive), ", ".join(opinion_negative), sentence))

	if _DEBUG_:
		print query

	cur = conn.cursor()
	cur.execute(query)
	cur.close()

def do_one(conn, news_url, news_title, news_content, positive_words, negative_words, companies):
	(title_positive_score, title_positive_data) = get_opinion_data(news_title, positive_words, 3)
	(title_negative_score, title_negative_data) = get_opinion_data(news_title, negative_words, 3)
	(title_company_id, title_company, title_company_score) = get_most_frequent_company_data(news_title, companies)

	if title_company and title_positive_score - title_negative_score != 0 and (title_positive_data or title_negative_data):
		insert_to_db(conn, title_company_id, title_positive_score - title_negative_score, news_url, title_positive_data, title_negative_data, news_title)

	for sentence in split_sentence(news_content):
		(positive_score, positive_data) = get_opinion_data(sentence, positive_words)
		(negative_score, negative_data) = get_opinion_data(sentence, negative_words)
		(company_id, company, company_score) = get_most_frequent_company_data(sentence, companies)

		if company and positive_score - negative_score != 0 and (positive_data or negative_data):
			insert_to_db(conn, company_id, positive_score - negative_score, news_url, positive_data, negative_data, sentence)

def do(positive_words, negative_words, companies, limit=50):
	conn = pymysql.connect("211.110.140.229", "soma", "thak2013", "stock", 3306, autocommit=True)

	cur = conn.cursor()
	cur.execute("select news_url, news_title, news_content from NEWS order by news_date desc limit %d" % (limit))

	for (news_url, news_title, news_content) in cur:
		if _DEBUG_:
			print "%s [%s]" % (news_title, colored_text(news_url, "blue"))

		if not check_already_exists(conn, news_url):
			do_one(conn, news_url, news_title, news_content, positive_words, negative_words, companies)

	cur.close()