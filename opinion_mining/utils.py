# coding: utf-8

TEXT_ATTRIBUTES = dict(zip(["bold", "dark", "", "underline", "blink", "", "reverse", "concealed"], range(1, 9)))
TEXT_BACKGROUNDS = dict(zip(["black", "red", "green", "yellow", "blue", "magenta", "cyan", "white"], range(40, 48)))
TEXT_COLORS = dict(zip(["black", "red", "green", "yellow", "blue", "magenta", "cyan", "white"], range(30, 38)))
del TEXT_ATTRIBUTES[""]

def colored_text(text, color=None, bg=None, attrs=[]):
	format = "\033[%dm%s"

	if color:
		text = format % (TEXT_COLORS[color], text)
	if bg:
		text = format % (TEXT_BACKGROUNDS[bg], text)
	if attrs:
		for attr in attrs:
			text = format % (TEXT_ATTRIBUTES[attr], text)
	text += "\033[0m"

	return text

if __name__ == "__main__":
	print colored_text("--HELLO WORLD--", color="green", bg="yellow", attrs=["bold", "reverse"])